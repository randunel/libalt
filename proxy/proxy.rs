#[macro_use]
extern crate log;
extern crate mio;
extern crate logger;
extern crate libalt;

use libalt::socket::*;
use libalt::event::*;
use libalt::message::*;
use libalt::netable::*;
use libalt::state::Context;
use libalt::{poll_loop, read_all, PollLoopCmd};

use mio::{PollOpt, EventSet, Poll, Token};

use std::net::SocketAddr;
use std::str::FromStr;

fn main() {
   let _ = logger::init();

   // client <--> a proxy b <--> server
   let addr_a = SocketAddr::from_str("127.0.0.1:27280").unwrap();
   let addr_b = SocketAddr::from_str("127.0.0.1:27281").unwrap();
   let socket_a = Socket::new(&addr_a, Context::Server, default_decoder).unwrap();
   let socket_b = Socket::new(&addr_b, Context::Client, default_decoder).unwrap();

   let poll = Poll::new().unwrap();
   poll.register(&socket_a.rx,
                Token(1),
                EventSet::readable(),
                PollOpt::level())
      .unwrap();
   poll.register(&socket_b.rx,
                Token(2),
                EventSet::readable(),
                PollOpt::level())
      .unwrap();

   let server = SocketAddr::from_str("127.0.0.1:27276").unwrap();
   let mut client = None;
   let mods = [ev_mods];

   let _ = poll_loop(&poll,
                     &mut |ev: mio::Event| {
      match ev.token().0 {
         1 => {
            read_all(&socket_a.rx, |mut msg: Msg| {
               client = Some(msg.addr);
               for m in &mods {
                  msg = m(msg, Context::Client);
               }
               msg.addr = server;
               msg.port = 27281;
               debug!("client -> server {:?}", msg);
               socket_b.sx.send(msg).unwrap();
               Ok(PollLoopCmd::Continue)
            })
         }
         2 => {
            read_all(&socket_b.rx, |mut msg: Msg| {
               if let Some(addr) = client {
                  for m in &mods {
                     msg = m(msg, Context::Server);
                  }
                  msg.addr = addr;
                  msg.port = 27280;
                  debug!("server -> client {:?}", msg);
                  socket_a.sx.send(msg).unwrap();
               }
               Ok(PollLoopCmd::Continue)
            })
         }
         _ => {
            warn!("unk token");
            Ok(PollLoopCmd::Continue)
         }
      }
   });
}

fn ev_mods(msg: Msg, sender: Context) -> Msg {
   let mods = [lvl_40];

   match msg.downcast::<GameMsg>() {
      Ok(mut msg) => {
         msg.data.events(|ev| for m in &mods {
            m(ev, sender);
         });
         msg.upcast()
      }
      Err(msg) => msg,
   }
}

fn lvl_40(ev: &mut Event, _: Context) {
   match *ev {
      Event::PlayerInfo(ref mut e) => e.level = u6::from(39),
      _ => (),
   }
}
