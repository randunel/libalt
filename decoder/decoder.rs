#[macro_use]
extern crate log;
extern crate logger;
extern crate libalt;
extern crate libvapor;

use libalt::message::*;
use libalt::state::*;
use libalt::socket::default_decoder;
use libvapor::vapor_decoder;

use std::io::Read;
use std::fs::File;
use std::net::SocketAddr;
use std::str::FromStr;
use std::env;

fn main() {
   let _ = logger::init();
   let args: Vec<String> = env::args().collect();

   if args.len() < 3 {
      info!("Usage: decoder <default|vapor> <file> (server|client)");
      return;
   }

   let decoder = match &args[1][..] {
      "default" => default_decoder,
      "vapor" => vapor_decoder,
      x => {
         error!("Unknown decoder {}, must be 'default' or 'vapor'", x);
         return;
      }
   };

   let context = if args.len() > 3 {
      match &args[3][..]{
         "client" => Context::Client,
         "server" => Context::Server,
         x => {
            error!("Unknown context {}, must be 'client' or 'server'", x);
            return;
         }
      }
   } else {
      Context::Client
   };

   let addr = SocketAddr::from_str("0.0.0.0:0").unwrap();
   let port = 0;
   let mut state = MsgState {
      context: context,
      shared: SharedState::new(),
   };

   let mut file = File::open(&args[2]).expect("Failed to open message file");
   let mut data = Vec::new();
   file.read_to_end(&mut data).expect("Failed to read file data");

   let msg = Msg::decode(data, addr, port, &mut state, decoder);

   info!("{:#?}", msg);
}
