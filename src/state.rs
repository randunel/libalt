extern crate libaltx;
extern crate serde_json;
pub use self::libaltx::map::*;

use math::*;
use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};
use std::fs::{self, File};
use std::io::Read;

/// The context of a message being read, server or client.
///
/// Some things in the altitude message format are different depening
/// on if it's a client or server sending/receiving the message, this
/// enum is used in packets to give this information to the read/write
/// functions of [Netables](../trait.Netable.html)
#[derive(Debug,Clone,Copy,PartialEq,Eq)]
pub enum Context {
   Client,
   Server,
}

#[derive(Debug,Clone)]
pub struct MsgState {
   pub context: Context,
   pub shared: SharedState,
}

impl MsgState {
   pub fn new() -> MsgState {
      MsgState {
         context: Context::Server,
         shared: SharedState::new(),
      }
   }
}

#[derive(Debug)]
pub struct State {
   // TODO: NetID map
   // TODO: Plane entity types for abilities
   pub maps: Vec<Map>,
   current_map: Option<usize>,
   map_size: Vec2f,
}

#[derive(Clone, Debug)]
pub struct SharedState {
   x: Arc<RwLock<State>>,
}

impl State {
   pub fn new() -> State {
      State {
         maps: Vec::new(),
         current_map: None,
         map_size: Vec2f::new(3000., 1500.),
      }
   }

   pub fn load_maps(&mut self, path: &str) {
      self.maps.clear();
      match fs::read_dir(path) {
         Ok(dir) => {
            for f in dir {
               let f = f.unwrap();
               let file_name = f.file_name().into_string().unwrap();
               if file_name.ends_with(".map") {
                  info!("Loading map {}", file_name);
                  let mut file = File::open(f.path()).unwrap();
                  let mut json = String::new();
                  if file.read_to_string(&mut json).is_ok() {
                     match serde_json::from_str::<Map>(&json) {
                        Ok(map) => self.maps.push(map),
                        Err(e) => warn!("Error parsing map {:?}: {}", f.path(), e),
                     }
                  }
               }
            }
         }
         Err(e) => warn!("Failed to open maps direcotry: {}", e),
      }
   }

   pub fn current_map(&self) -> Option<&Map> {
      if let Some(map) = self.current_map {
         if self.maps.len() <= map {
            return None;
         }
         return Some(&self.maps[map]);
      }
      None
   }

   pub fn set_map(&mut self, name: &str) {
      for i in 0..self.maps.len() {
         if self.maps[i].name == name {
            self.current_map = Some(i);
            let s = self.maps[i].size();
            self.map_size = Vec2f::new(s[0] as f32, s[1] as f32);
            break;
         }
      }
      warn!("unknown map '{}'", name);
      self.map_size = Vec2f::new(3000., 1500.);
   }

   pub fn map_size(&self) -> Vec2f {
      return self.map_size;
   }
}

impl SharedState {
   pub fn new() -> SharedState {
      SharedState { x: Arc::new(RwLock::new(State::new())) }
   }
   pub fn read(&self) -> RwLockReadGuard<State> {
      match self.x.read() {
         Ok(guard) => guard,
         Err(poisoned) => poisoned.into_inner(),
      }
   }
   pub fn write(&self) -> RwLockWriteGuard<State> {
      match self.x.write() {
         Ok(guard) => guard,
         Err(poisoned) => poisoned.into_inner(),
      }
   }
}
