/// Netable structures for information about a server

use bitstream::*;
use meta::*;
use netable::*;

use std::net::SocketAddr;
use std::default::Default;

/// Server information to be displayed on the server list.
/// The `disallow_demo` flag has been disued since the game was made free, it
/// can still be used, but has no effect as there are no demo players.
#[derive(Debug,Clone)]
pub struct ServerInfo {
   pub id: u16,
   pub addr: Option<SocketAddr>,
   pub map_name: Option<String>,
   pub max_players: u16,
   pub num_players: u16,
   pub server_name: Option<String>,
   pub pass_req: bool,
   pub hardcore: bool,
   pub min_level: i32,
   pub max_level: i32,
   pub disallow_demo: bool,
   pub version: Version,
}

/// Changes how gravity works, as in the testGravityMode command:
/// Normal - Everything is affected by gravity, as usual.
#[derive(Debug,Clone,Copy)]
pub enum GravMode {
   Normal = 0,
   One,
   Two,
   Three,
}
impl Default for GravMode {
   fn default() -> GravMode {
      GravMode::Three
   }
}

/// Optionaly disable weapons
#[derive(Debug,Clone,Copy)]
pub enum WeaponMode {
   Normal = 0,
   NoPrimary,
   NoSecondary,
   None,
}
impl Default for WeaponMode {
   fn default() -> WeaponMode {
      WeaponMode::Normal
   }
}

/// An incomplete implementation of the server configuration, as sent
/// to clients on load.
#[derive(Default,Debug,Clone)]
pub struct ServerConf {
   pub map_source: String,
   pub test_em: f32,
   pub health_mod: f32,
   pub test_ds: bool,
   pub scale: u32,
   pub plane_scale: u32,
   pub grav: GravMode,
   pub weapons: WeaponMode,
   pub unknown1: u32,
   pub unknown2: u32,
   // TODO: map & command lists go here
   // TODO: game mode configs
   pub prevent_switch: bool,
   pub no_bal_msg: bool,
   pub unknown3: bool,
   pub max_players: u16,
   pub max_ping: u32,
   pub unknown4: bool,

   pub counter: u32,
   pub data: Option<(bool, Vec<u8>)>,
}

impl ServerConf {
   pub fn new() -> ServerConf {
      let mut ret = ServerConf::default();
      ret.test_em = 1f32;
      ret.health_mod = 1f32;
      ret.scale = 100u32;
      ret.plane_scale = 100u32;
      ret
   }
}


impl<T: RW> Netable<T> for ServerInfo {
   fn read(_p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self> {
      unimplemented()
   }
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      try!(p.write_u16(self.id));
      try!(self.addr.write(p, state));
      try!(self.map_name.write(p, state));
      try!(p.write(8, self.max_players as u64));
      try!(p.write(8, self.num_players as u64));
      try!(self.server_name.write(p, state));
      try!(p.write_bool(self.pass_req));
      try!(p.write_bool(self.hardcore));
      try!(p.write_range(1, 60, self.min_level));
      try!(p.write_range(1, 60, self.max_level));
      try!(p.write_bool(self.disallow_demo));
      try!(self.version.write(p, state));
      Ok(())
   }
}

impl Default for ServerInfo {
   fn default() -> Self {
      ServerInfo {
         id: 1,
         addr: None,
         map_name: Some("none".to_owned()),
         max_players: 10,
         num_players: 0,
         server_name: Some("Altserv".to_owned()),
         pass_req: false,
         hardcore: true,
         min_level: 1,
         max_level: 60,
         disallow_demo: false,
         version: VERSION,
      }
   }
}

impl<T: RW> Netable<T> for ServerConf {
   fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self> {
      let compressed = try!(p.read_bool());
      let mut data = Vec::new();
      let len = try!(p.read(12));
      for _ in 0..len {
         data.push(try!(p.read_u8()))
      }
      let mut conf = ServerConf::new();
      conf.data = Some((compressed, data));
      Ok(conf)
   }
   fn write(&self, pkt: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      if let Some((ref compressed, ref data)) = self.data {
         try!(pkt.write_bool(*compressed));
         try!(pkt.write(12, data.len() as u64));
         try!(pkt.write_bytes(data));
         return Ok(());
      }

      let mut p = BitStreamVec::new();
      let mut pb = BitStreamVec::new();
      pb.le = true;

      try!(pb.write_str(&self.map_source[..]));
      try!(pb.write_f32(self.test_em));
      try!(pb.write_f32(self.health_mod));
      try!(pb.write_bool(self.test_ds));
      try!(pb.write_buf());
      try!(p.write_bytes(&pb.into_vec()));

      try!(p.write_range(40, 300, self.scale as i32));
      try!(p.write_range(40, 300, self.plane_scale as i32));

      try!(p.write_range(0, 3, self.grav as i32));
      try!(p.write_range(0, 3, self.weapons as i32));
      try!(p.write_range(0, 1000, self.unknown1 as i32));
      try!(p.write_range(0, 1000, self.unknown2 as i32));

      try!(p.write_u32(0));
      try!(p.write_u32(0));
      try!(p.write_u32(0));

      try!(p.write_str_l("<ffa/>"));
      try!(p.write_str_l("<tbd/>"));
      try!(p.write_str_l("<obj/>"));
      try!(p.write_str_l("<ball/>"));
      try!(p.write_str_l("<tdm/>"));

      try!(p.write_bool(self.prevent_switch));
      try!(p.write_bool(self.no_bal_msg));
      try!(p.write_bool(self.unknown3));
      try!(p.write_range(0, 63, self.max_players as i32));
      try!(p.write_range(0, 1000, self.max_ping as i32));
      try!(p.write_bool(self.unknown4));

      try!(p.write_buf());

      try!(pkt.write_bool(false)); //compressed
      try!(pkt.write(12, p.len()));
      try!(pkt.write_bytes(&p.into_vec()));
      Ok(())
   }
}
