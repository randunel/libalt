//! An implementation of the networkinng protocol of Altitude, a competitive
//! 2D plane game.

#[macro_use]
extern crate log;
#[macro_use]
extern crate mopa;
#[macro_use]
extern crate custom_derive;
#[macro_use]
extern crate newtype_derive;
extern crate mio;
extern crate rand;
extern crate math;
extern crate bitstream;

#[macro_use]pub mod netable;
#[macro_use]pub mod map_vec;

pub mod client;
pub mod socket;
pub mod event;
pub mod entity;
pub mod event_mgr;
pub mod guarantee;
pub mod meta;
pub mod message;
pub mod server;
pub mod state;
pub mod vapor;

use std::sync::mpsc::TryRecvError;

pub enum PollLoopCmd {
   Stop,
   Continue,
}

/// Loop over events in a mio::Poll, passing each event to the given closure
/// The closure returns a result - if successful it returns a command
/// (Stop or Continue) for the loop, otherwise an Error of type E.
/// Returns any error the given closure returns, or Ok(()) on Stop.
pub fn poll_loop<F, E>(poll: &mio::Poll, mut f: F) -> Result<(), E>
   where F: FnMut(mio::Event) -> Result<PollLoopCmd, E>
{
   let mut events = mio::Events::new();
   loop {
      match poll.poll(&mut events, None) {
         Err(e) => error!("Poll failed, continuing: {:?}", e),
         Ok(num) => {
            for i in 0..num {
               if let Some(ev) = events.get(i) {
                  match f(ev) {
                     Ok(PollLoopCmd::Continue) => (),
                     Ok(PollLoopCmd::Stop) => return Ok(()),
                     Err(e) => return Err(e),
                  }
               }
            }
         }
      }
   }
}

/// Read all the new data on a channel, passing each new piece of data
/// to the given closure.
/// If the receiver is disconnected, returns an error.
pub fn read_all<T, F>(rx: &mio::channel::Receiver<T>, mut f: F) -> Result<PollLoopCmd, String>
   where F: FnMut(T) -> Result<PollLoopCmd, String>
{
   loop {
      match rx.try_recv() {
         Ok(x) => {
            match f(x) {
               Ok(PollLoopCmd::Continue) => (),
               Ok(PollLoopCmd::Stop) => return Ok(PollLoopCmd::Stop),
               Err(e) => return Err(e),
            }
         }
         Err(e) => {
            match e {
               TryRecvError::Empty => break,
               TryRecvError::Disconnected => {
                  return Err("Receiver disconnected.".to_string());
               }
            }
         }
      }
   }
   Ok(PollLoopCmd::Continue)
}
