use bitstream::*;
use std::io::{self, Read, Write, Seek};

pub use state::MsgState;

/// A combination trait, std::io Read and Write
pub trait RW: Read + Write {}
impl<T: Read + Write> RW for T {}

/// A combination trait, std::io Read, Write and Seek,
pub trait RWS: Read + Write + Seek {}
impl<T: Read + Write + Seek> RWS for T {}


/// Return an IO error with the string "unimplemented"
/// Used as a placeholder while adding parts of new netable structures.
pub fn unimplemented<T>() -> io::Result<T> {
   Err(io::Error::new(io::ErrorKind::Other, "unimplemented"))
}

/// A trait allowing structures to be read and written from/to streams.
/// The type parameter specifies the type of stream the structure needs,
/// usually either RW (Read + Write) or RWS (.. + Seek)
pub trait Netable<T>: Sized {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self>;
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()>;
}

/// An automatic implementation of Netable for structs containing only other
/// Netable types. To be used with custom_derive.
/// All struct fields must be public, and doc comments will fail to parse.
#[macro_export]
macro_rules! Netable {
    // Implement the read fn for the given struct
    (@impl_read $name:ident { $(pub $var:ident : $t:ty),* $(,)* }) => {
        fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<$name>{
            Ok($name{
                $( $var: {
                    let v = try!(<$t>::read(p, state));
                    trace!{"read {:?}",v};
                    v
                } ),*
            })
        }
    };
    // Implement write for the struct
    (@impl_write { $(pub $var:ident : $t:ty),* $(,)* }) => {
        fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()>{
            $( try!(self.$var.write(p, state));)*
            Ok(())
        }
    };

    // Parse the struct in the form given by custom_derive, and implement
    // the Netable trait
    // TODO:
    // Recursive for smaller match patterns - change this for better error
    // messages when using from a different crate? This probably isn't needed
    // if I'm moving libvapor into libalt.
    (($typ:ident) $(pub)* struct $name:ident $tail:tt) => {
        impl<T: $typ> Netable<T> for $name {
            Netable! {@impl_read $name $tail}
            Netable! {@impl_write $tail}
        }
    };
}

/// Implement Netable for an enum.
#[macro_export]
macro_rules! enum_netable {
    ($name:ident; $bits:expr; $default:ident = $def_v:expr,
     $($n:ident = $value:expr),*) => {
        impl<T:RW> Netable<T> for $name {
        fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<$name>{
            match try!(p.read($bits)){
                $($value => Ok($name::$n)),*,
                _ => Ok($name::$default)
            }
        }
        fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()>{
            match self{
                $(&$name::$n => try!(p.write($bits, $value as u64))),*,
                &$name::$default => try!(p.write($bits, $def_v as u64))
            } Ok(())
        }
        }
    };
}

/// Implement netable for an empty placeholder struct
#[macro_export]
macro_rules! empty_netable{
    ($name:ident) => {
        impl<T:RW> Netable<T> for $name{
        fn read(_p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self>{
            use std::default::Default;
            Ok($name::default())}
        fn write(&self, _p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()>{
            Ok(())}
        }
    }
}
#[derive(Clone,Copy,Default,Debug)]
pub struct EmptyNet;
empty_netable!(EmptyNet);

/// Create a new numeric struct, using custom_derive to implement some
/// traits.
macro_rules! custom_num{
    ($name:ident, $t:ty) => {
        custom_derive!{
            #[allow(non_camel_case_types)]
            #[derive(Debug, Clone, Copy, NewtypeFrom, NewtypeDeref,
                     NewtypeAdd)]
            pub struct $name($t);
        }
    }
}

/// Create an integer wrapper struct which implements Netable for a given range,
/// using the minimum required bits
macro_rules! range_netable{
    ($name:ident, $min:expr, $max:expr) => {
        custom_num!($name, i32);
        impl<T:RW> Netable<T> for $name{
        fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self>{
            Ok($name::from(try!(p.read_range($min, $max)))) }
        fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()>{
            p.write_range($min, $max, **self as i32) }
        }
    }
}

/// Create a numeric struct which implements Netable for a scaled range,
/// using the minimum required bits
macro_rules! srange_netable{
    ($name:ident, $min:expr, $max:expr, $scale:expr) => {
        custom_num!($name, f32);
        impl<T:RW> Netable<T> for $name{
        fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self>{
            Ok($name::from(try!(p.read_srange($min, $max, $scale)))) }
        fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()>{
            p.write_srange($min, $max, $scale, **self as f32) }
        }
    }
}

macro_rules! nbit{
    ($name:ident, $bits:expr, $t:ty) => {
        custom_num!($name, $t);
        impl<T: RW> Netable<T> for $name{
        fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self>{
            Ok($name::from(try!(p.read($bits)) as $t))
        }
        fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()>{
            p.write($bits, **self as u64)
        }
        }
    }
}

range_netable!(Angle, -180, 180);

nbit!(u2, 2, u8);
nbit!(u5, 5, u8);
nbit!(u6, 6, u8);
nbit!(u7, 7, u8);

nbit!(u10, 10, u16);
nbit!(u11, 11, u16);
nbit!(u12, 12, u16);

impl<A: RW, T: Netable<A>> Netable<A> for Option<T> {
   fn read(p: &mut BitStream<A>, state: &mut MsgState) -> Res<Self> {
      let b = !(try!(p.read_bool()));
      Ok(if b {
         Some(try!(T::read(p, state)))
      } else {
         None
      })
   }
   fn write(&self, p: &mut BitStream<A>, state: &mut MsgState) -> Res<()> {
      match *self {
         None => try!(p.write_bool(true)),
         Some(ref x) => {
            try!(p.write_bool(false));
            try!(x.write(p, state));
         }
      }
      Ok(())
   }
}

macro_rules! impl_prim{
    ($name:ident, $read:ident, $write:ident) => {
        impl<T:RW> Netable<T> for $name{
        fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self>{
            Ok(try!(p.$read())) }
        fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()>{
            p.$write(*self) }
        }
    }
}

// Implement Netable for a bunch of primitives
impl_prim!(bool, read_bool, write_bool);
impl_prim!(i8, read_i8, write_i8);
impl_prim!(i16, read_i16, write_i16);
impl_prim!(i32, read_i32, write_i32);
impl_prim!(i64, read_i64, write_i64);
impl_prim!(u8, read_u8, write_u8);
impl_prim!(u16, read_u16, write_u16);
impl_prim!(u32, read_u32, write_u32);
impl_prim!(u64, read_u64, write_u64);

impl<T: RW> Netable<T> for String {
   fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self> {
      p.read_str()
   }
   fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      p.write_str(self)
   }
}

/// Implement Netable for a newly created Vec wrapper struct, which
/// reads/writes the given number of bits for the size of the vec
#[macro_export]
macro_rules! vec_netable{
    ($name:ident, $len_bits:expr, $t:ty) => {
        custom_derive!{
            #[derive(Debug, Clone, NewtypeFrom, NewtypeDeref)]
            pub struct $name(Vec<$t>);
        }
        impl<T:RW> Netable<T> for $name{
        fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self>{
            let mut vec = Vec::new();
            for _ in 0..try!(p.read($len_bits)){
                vec.push(try!(<$t>::read(p, state)));
            }
            Ok($name::from(vec))
        }
        fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()>{
            try!(p.write($len_bits, self.len() as u64));
            for i in &**self{
                try!(i.write(p, state));
            } Ok(())
        }
        }
    }
}

impl<A: RW, T: Netable<A>> Netable<A> for Vec<T> {
   fn read(p: &mut BitStream<A>, state: &mut MsgState) -> Res<Self> {
      let mut vec = Vec::new();
      for i in 0..try!(p.read_i32()) {
         trace!("vec_netable: reading item {}", i);
         vec.push(try!(T::read(p, state)));
      }
      Ok(vec)
   }
   fn write(&self, p: &mut BitStream<A>, state: &mut MsgState) -> Res<()> {
      try!(p.write_i32(self.len() as i32));
      for i in self {
         try!(i.write(p, state));
      }
      Ok(())
   }
}

/// Create a new enum which implements Netable
#[macro_export]
macro_rules! multimsg{
    ($e:ident, $bits:expr, $traits:ident; $($header:expr, $name:ident $entity:ty);*;) => {
        #[derive(Debug,Clone)]
        pub enum $e{ $($name($entity)),* }

        impl<T: $traits> Netable<T> for $e{
            fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self>{
                let header = try!(p.read($bits));
                let res = match header{
                    $($header => $e::$name(try!(<$entity>::read(p,state)))),*,
                    x => {
                        error!("Unimplmented {} header: {}",
                               stringify!($e).to_lowercase(), x);
                        return unimplemented();
                    }
                };
                trace!("read {:?}",res);
                Ok(res)
            }
            fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()>{
                match *self{
                    $($e::$name(ref x) => {
                        try!(p.write($bits,$header));
                        try!(x.write(p,state)); })*
                } Ok(())
            }
        }
    };
}
