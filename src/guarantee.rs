//! An implementation of altitude's transport protocol.
//!
//! Altitude uses UDP, with optional guarantees for message delivery
//! and order. This module contains a representation of the
//! guarantee data, and a Connection struct, which maintains a guaranteed
//! connection.
//! The implementation of Connection is abstracted from the network layer -
//! it's functions return vectors of messages to be sent and dispatched.

use netable::*;
use bitstream::*;
use message::*;

use std::collections::{HashMap, HashSet};
use std::time::*;
use rand;

/// The guarantee level.
#[derive(Clone,Copy,Debug,PartialEq)]
pub enum GuaranteeLevel {
   None = 0,
   Delivery,
   Order,
   Ping,
}

/// Guarantee data, part of a packet's header
#[derive(Clone,Debug,PartialEq)]
pub enum GuaranteeData {
   Open {
      id: i32,
      is_open: bool,
      data: Box<GuaranteeData>,
   },
   OpenAck {
      id: i32,
   },
   Data {
      seq: u16,
   },
   DataAck {
      seq: u16,
   },
   None,
}

#[derive(Clone,Debug,PartialEq)]
pub struct Guarantee {
   pub level: GuaranteeLevel,
   pub data: GuaranteeData,
}

/// The status of a connection.
/// Sending: The connection is currently sending data, for which it hasn't yet
/// received an acknowledgement.
/// Idle: The connection is still active, data has been either sent or received
/// recently
/// Expired: The connection has expired - the object should be deleted.
#[derive(PartialEq,Debug,Clone,Copy)]
pub enum Status {
   Sending,
   Idle,
   Expired,
}

/// A Connection to a single client/server.
pub struct Connection {
   rec_id: Option<i32>,
   last_rec: Instant,
   rec_seq: u32,
   last_disp: u32,
   missing: HashSet<u32>,
   order_buf: HashMap<u32, Message>,

   last_send: Instant,
   snd_id: Option<i32>,
   snd_open: bool,
   snd_queue: HashMap<u32, SendData>,
   snd_seq: u32,
}

/// Estimate a 32-bit sequence number given it's 16-bit truncated form and
/// the last known sequence number.
pub fn get_seq(seq: u16, prev: u32) -> u32 {
   let mut seq = seq as u32;
   let a = 1 << 16;
   while seq < prev {
      seq += a
   }
   if seq > prev + a / 2 {
      seq -= a
   }
   seq
}
/// Truncate a sequence number to 16 bits
pub fn make_seq(seq: u32) -> u16 {
   seq as u16
}

struct SendData {
   last_send: Instant,
   attempts: i64,
   message: Message,
}

impl Connection {
   /// Create a new connection
   pub fn new() -> Connection {
      let now = Instant::now();
      Connection {
         rec_id: None,
         last_rec: now,
         rec_seq: 0,
         last_disp: 0,
         missing: HashSet::new(),
         order_buf: HashMap::new(),

         last_send: now,
         snd_id: None,
         snd_open: false,
         snd_queue: HashMap::new(),
         snd_seq: 0,
      }
   }

   /// Attempts to recreate the connection, re-sending the data
   /// in the send queue with the open request.
   pub fn reset(&mut self) -> (Vec<Message>, Vec<Message>) {
      unimplemented!();
   }

   /// Get the status of the connection
   pub fn status(&self, now: Instant) -> Status {
      if now.duration_since(self.last_rec) > Duration::from_secs(20) {
         if now.duration_since(self.last_send) > Duration::from_secs(20) {
            return Status::Expired;
         }
      }
      if self.snd_queue.len() > 0 {
         Status::Sending
      } else {
         Status::Idle
      }
   }

   /// Pass a received message to the connection for processing.
   /// Returns two vectors of messages, the first set to be dispatched
   /// internally, the second set to be encoded and sent over the socket
   pub fn receive(&mut self,
                  mut msg: Message,
                  data: Option<GuaranteeData>)
                  -> (Vec<Message>, Vec<Message>) {

      let mut dispatch = Vec::new();
      let mut send = Vec::new();

      let data = match data {
         Some(x) => x,
         None => msg.guarantee.data.clone(),
      };

      match data {
         GuaranteeData::Open { id, is_open, data } => {
            debug!("got open request from {}: id: {}, is_open: {}",
                   msg.addr,
                   id,
                   is_open);
            // TODO: check if ids are equal if already open
            self.rec_id = Some(id);
            debug!("sending open ack, id: {}", id);
            let mut m = msg.reply(Empty);
            m.guarantee.data = GuaranteeData::OpenAck { id: id };
            send.push(m);
            let (mut d, mut s) = self.receive(msg, Some(*data));
            send.append(&mut s);
            dispatch.append(&mut d);
         }
         GuaranteeData::OpenAck { id } => {
            self.snd_open = id != 1;
            if id == 1 {
               let (mut d, mut s) = self.reset();
               send.append(&mut s);
               dispatch.append(&mut d);
            }
         }
         GuaranteeData::Data { seq } => {
            if self.rec_id.is_none() {
               debug!("got data without a connection, sending openack(1)");
               let mut m = msg.reply(Empty);
               m.guarantee.data = GuaranteeData::OpenAck { id: 1 };
               send.push(m);
            } else {
               let seq = get_seq(seq, self.rec_seq);
               self.last_rec = Instant::now();

               debug!("sending data ack for {}", seq);
               let mut m = msg.reply(Empty);
               m.guarantee.data = GuaranteeData::DataAck { seq: make_seq(seq) };
               send.push(m);

               let new = seq > self.rec_seq;
               let missing = self.missing.contains(&seq);
               if new {
                  for i in self.rec_seq + 1..seq {
                     debug!("waiting for seq {}", i);
                     self.missing.insert(i);
                  }
                  self.rec_seq = seq;
               } else if missing {
                  self.missing.remove(&seq);
               }

               msg.guarantee.data = GuaranteeData::None;

               if new || missing {
                  match msg.guarantee.level {
                     GuaranteeLevel::Delivery => {
                        dispatch.push(msg);
                     }
                     GuaranteeLevel::Order => {
                        if self.last_disp + 1 == seq {
                           dispatch.push(msg);
                           self.last_disp += 1;
                        } else {
                           self.order_buf.insert(seq, msg);
                        }
                        loop {
                           let i = self.last_disp + 1;
                           if !self.order_buf.contains_key(&i) {
                              break;
                           }
                           let m = self.order_buf
                              .remove(&i)
                              .expect("failed to move from order_buf");
                           dispatch.push(m);
                           self.last_disp += 1;
                        }
                     }
                     _ => {}
                  }
               }
            }
         }
         GuaranteeData::DataAck { seq } => {
            let seq = get_seq(seq, self.snd_seq);
            debug!("got data ack for {}, removing from queue", seq);
            self.snd_queue.remove(&seq);
         }
         _ => {
            warn!("No guarantee data, ignoring packet.");
         }
      }

      (dispatch, send)
   }

   /// Checks the messages currently being sent, and re-send them if needed.
   /// Returns a vector of pre-encoded messages to be sent through a socket
   pub fn update(&mut self, now: Instant) -> Vec<&Message> {
      let mut to_remove = Vec::new();
      let mut to_send = Vec::new();

      for (seq, m) in &mut self.snd_queue {
         if m.attempts > 10 {
            to_remove.push(*seq);
            continue;
         }
         let since_sent = now.duration_since(m.last_send);
         if since_sent > Duration::from_millis(400) {
            m.last_send = now;
            m.attempts += 1;
            debug!("sending guaranteed data, seq: {}", seq);
            to_send.push(*seq);
         }
      }
      for s in to_remove {
         self.snd_queue.remove(&s);
      }
      if to_send.len() > 0 {
         self.last_send = now;
      }

      let mut send = Vec::new();
      for s in to_send {
         send.push(&self.snd_queue.get(&s).unwrap().message);
      }
      send
   }

   /// Send the given message. The message's guarantee data is set, including
   /// a request to open a connection if needed, the message is added to the
   /// queue, and a reference to it returned to be sent through a socket.
   pub fn send(&mut self, mut msg: Message) -> &Message {
      // TODO: convert to Data::Raw here?
      self.snd_seq += 1;
      let data = GuaranteeData::Data { seq: make_seq(self.snd_seq) };

      msg.guarantee = if !self.snd_open {
         if self.snd_id.is_none() {
            let mut id = 0i32;
            while (id >= 0) && (id <= 16) {
               id = rand::random();
            }
            self.snd_id = Some(id);
         }
         Guarantee {
            level: msg.guarantee.level,
            data: GuaranteeData::Open {
               id: self.snd_id.unwrap(),
               is_open: self.rec_id.is_some(),
               data: Box::new(data),
            },
         }
      } else {
         Guarantee {
            level: msg.guarantee.level,
            data: data,
         }
      };
      self.last_send = Instant::now();
      self.snd_queue.insert(self.snd_seq,
                            SendData {
                               last_send: Instant::now(),
                               attempts: 1,
                               message: msg,
                            });
      let s = self.snd_seq;
      debug!("sending guaranteed data, seq: {}", s);
      &self.snd_queue.get(&s).unwrap().message
   }
}

impl<T: RW> Netable<T> for GuaranteeLevel {
   fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self> {
      let t = try!(p.read(2));
      Ok(match t {
         0 => GuaranteeLevel::None,
         1 => GuaranteeLevel::Delivery,
         2 => GuaranteeLevel::Order,
         3 => GuaranteeLevel::Ping,
         _ => unreachable!(),
      })
   }
   fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      p.write(2, *self as u64)
   }
}

impl<T: RW> Netable<T> for GuaranteeData {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self> {
      let t = try!(p.read(2));
      Ok(match t {
         0 => {
            GuaranteeData::Open {
               id: try!(p.read_i32()),
               is_open: try!(p.read_bool()),
               data: Box::new(try!(GuaranteeData::read(p, state))),
            }
         }
         1 => GuaranteeData::OpenAck { id: try!(p.read_i32()) },
         2 => GuaranteeData::Data { seq: try!(p.read_u16()) },
         3 => GuaranteeData::DataAck { seq: try!(p.read_u16()) },
         _ => unreachable!(),
      })
   }

   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      match *self {
         GuaranteeData::Open { ref id, ref is_open, ref data } => {
            try!(p.write(2, 0));
            try!(p.write_i32(*id));
            try!(p.write_bool(*is_open));
            try!(data.write(p, state));
         }
         GuaranteeData::OpenAck { ref id } => {
            try!(p.write(2, 1));
            try!(p.write_i32(*id));
         }
         GuaranteeData::Data { ref seq } => {
            try!(p.write(2, 2));
            try!(p.write_u16(*seq));
         }
         GuaranteeData::DataAck { ref seq } => {
            try!(p.write(2, 3));
            try!(p.write_u16(*seq));
         }
         GuaranteeData::None => {}
      }
      Ok(())
   }
}
