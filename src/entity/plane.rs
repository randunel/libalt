use bitstream::*;
use client::*;
use entity::Entity;
use map_vec::*;
use math::*;
use meta::*;
use netable::*;

pub type Loop = PlaneBase;
pub type Bomber = PlaneBase;
pub type Whale = PlaneBase;
pub type Bip = PlaneBase;
pub type Randa = PlaneBase;

#[derive(Debug,Clone)]
pub struct PlaneBase {
   player: PlayerNo,
   team: Team,
   setup: PlaneSetup,
   pos: Vec2f,
   angle: f32,
   powerup: Option<Box<Entity>>,
   unk1: bool,
   unk2: u16,
   unk3: u16,
   unk4: u8,
}

impl<T: RW> Netable<T> for PlaneBase {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<PlaneBase> {
      Ok(PlaneBase {
         player: try!(PlayerNo::read(p, state)),
         team: try!(Team::read(p, state)),
         setup: try!(PlaneSetup::read(p, state)),
         pos: try!(read_map_pos(p, state, 50., 1.)),
         angle: try!(p.read_range(-180, 180)) as f32,
         powerup: if try!(p.read_bool()) {
            Some(Box::new(try!(Entity::read(p, state))))
         } else {
            None
         },
         unk1: try!(p.read_bool()),
         unk2: try!(p.read(16)) as u16,
         unk3: try!(p.read(16)) as u16,
         unk4: try!(p.read_range(0, 90)) as u8,
      })
   }
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      try!(self.player.write(p, state));
      try!(self.team.write(p, state));
      try!(self.setup.write(p, state));
      try!(write_map_pos(p, state, 50., 1., self.pos));
      try!(p.write_range(-180, 180, self.angle as i32));
      match self.powerup {
         Some(ref pup) => {
            try!(p.write_bool(true));
            try!(pup.write(p, state));
         }
         None => try!(p.write_bool(false)),
      };
      try!(p.write_bool(self.unk1));
      try!(p.write_u16(self.unk2));
      try!(p.write_u16(self.unk3));
      try!(p.write_range(0, 90, self.unk4 as i32));
      Ok(())
   }
}
