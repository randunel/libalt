use bitstream::*;
use netable::*;

pub mod powerup;
pub mod plane;

use entity::powerup::*;
use entity::plane::*;

multimsg!( Entity, 6, RW;
    0,  Bip Bip;
    6,  Bomber Bomber;
    12, Whale Whale;
    16, Loop Loop;
    21, Randa Randa;

    30, Health Health;
    31, Wall Wall;
    32, Missile Missile;
    34, Shield Shield;
    37, Charge Charge;
    39, Ball Ball;
);
