use bitstream::*;
use client::*;
use math::*;
use map_vec::*;
use meta::*;
use netable::*;

pub type Shield = PowerupBase;
pub type Wall = PowerupBase;
pub type Missile = PowerupBase;
pub type Charge = PowerupBase;

#[derive(Debug,Clone)]
pub struct PowerupBase {
   pub unk1: bool,
   pub pos: Vec2f,
   pub vel: Vec2f,
   pub team: Team,
   pub c: Option<f32>,
   pub spawner: Option<NetID>,
   pub player: Option<PlayerNo>,
   pub d: Option<i32>,
}

range_netable!(HealthValue, 0, 100);
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct Health{
    pub powerup: PowerupBase,
    pub value: HealthValue,
    pub b: bool,
}}

#[derive(Debug,Clone)]
pub struct Ball {
   pub powerup: PowerupBase,
   pub carrier: Option<(Team, PlayerNo)>,
   pub b: bool,
   pub last_carriers: [PlayerNo; 3],
}

impl<T: RW> Netable<T> for PowerupBase {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self> {
      let a = try!(p.read_bool());
      let scale = if a {
         64f32
      } else {
         1f32
      };
      let pos = try!(read_map_pos(p, state, 300., scale));
      let vel_x = try!(p.read_srange(-50., 50., 10. * scale));
      let vel_y = try!(p.read_srange(-50., 50., 10. * scale));

      let team = try!(Team::read(p, state));
      let c = if !try!(p.read_bool()) {
         Some(try!(p.read_srange(0., 13. * 30., 1.)))
      } else {
         None
      };

      // let spawner = Option::<NetID>::read(p);
      let spawner = if try!(p.read_bool()) {
         Some(try!(NetID::read(p, state)))
      } else {
         None
      };

      let (player, d) = if try!(p.read_bool()) {
         (Some(try!(PlayerNo::read(p, state))), Some(try!(p.read_range(0, 300))))
      } else {
         (None, None)
      };

      Ok(PowerupBase {
         unk1: a,
         pos: pos,
         vel: Vec2f::new(vel_x, vel_y),
         team: team,
         c: c,
         spawner: spawner,
         player: player,
         d: d,
      })
   }
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      try!(p.write_bool(self.unk1));
      let scale = if self.unk1 {
         64f32
      } else {
         1f32
      };
      try!(write_map_pos(p, state, 300., scale, self.pos));
      try!(p.write_srange(-50., 50., 10. * scale, self.vel.x));
      try!(p.write_srange(-50., 50., 10. * scale, self.vel.y));

      try!(self.team.write(p, state));
      if let Some(c) = self.c {
         try!(p.write_bool(true));
         try!(p.write_srange(0., 13. * 30., 1., c));
      } else {
         try!(p.write_bool(false));
      }

      try!(self.spawner.write(p, state));

      if let Some(pl) = self.player {
         try!(p.write_bool(true));
         try!(pl.write(p, state));
         try!(p.write_range(0, 300, self.d.unwrap()));
      } else {
         try!(p.write_bool(false));
      }
      Ok(())
   }
}

impl<T: RW> Netable<T> for Ball {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Ball> {
      Ok(Ball {
         powerup: try!(PowerupBase::read(p, state)),
         carrier: if try!(p.read_bool()) {
            Some((try!(Team::read(p, state)), try!(PlayerNo::read(p, state))))
         } else {
            None
         },
         b: try!(p.read_bool()),
         last_carriers: [try!(PlayerNo::read(p, state)),
                         try!(PlayerNo::read(p, state)),
                         try!(PlayerNo::read(p, state))],
      })
   }
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      try!(self.powerup.write(p, state));
      if let Some((team, player)) = self.carrier {
         try!(team.write(p, state));
         try!(player.write(p, state));
      } else {
         try!(p.write_bool(false));
      }
      try!(p.write_bool(self.b));
      for c in &self.last_carriers {
         try!(c.write(p, state));
      }
      Ok(())
   }
}
