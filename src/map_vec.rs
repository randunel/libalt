/// Map position vector utilities
/// The position of most entities is encoded dependent on the size of the
/// currently open map - the fewest bits possible are used. This module
/// contains functions to read and write these positions, and a macro
/// to generate Netable position vector structs.

use bitstream::*;
use math::*;
use netable::*;

/// Get the size of the current map from shared state
pub fn get_map_size(shared: &::state::SharedState) -> Vec2f {
   let lock = shared.read();
   lock.map_size()
}

/// Read a scaled map position vector
pub fn read_map_pos<T: RW>(p: &mut BitStream<T>,
                           state: &mut MsgState,
                           pad: f32,
                           scale: f32)
                           -> Res<Vec2f> {
   let map_size = get_map_size(&state.shared);
   let pos_x = try!(p.read_srange(-pad, map_size.x + pad, scale));
   let pos_y = try!(p.read_srange(-pad, map_size.y + pad, scale));
   Ok(Vec2f::new(pos_x, pos_y))
}

/// Write a scaled map position vector
pub fn write_map_pos<T: RW>(p: &mut BitStream<T>,
                            state: &mut MsgState,
                            pad: f32,
                            scale: f32,
                            vec: Vec2f)
                            -> Res<()> {
   let map_size = get_map_size(&state.shared);
   try!(p.write_srange(-pad, map_size.x + pad, scale, vec.x));
   p.write_srange(-pad, map_size.y + pad, scale, vec.y)
}

macro_rules! map_pos{
    ($name:ident, $pad:expr, $scale:expr) => {
        custom_derive!{
            #[derive(Debug, Clone, Copy, NewtypeFrom, NewtypeDeref,
                     NewtypeAdd)]
            pub struct $name(Vec2f);
        }
        impl<T:RW> Netable<T> for $name{
        fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self>{
            Ok($name::from(try!(read_map_pos(p, state, $pad, $scale)))) }
        fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()>{
            write_map_pos(p, state, $pad, $scale, **self) }
        }
    }
}
