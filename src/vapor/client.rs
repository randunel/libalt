use mio;
use mio::Poll;
use mio::channel::*;
use std::net::*;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;
use vapor::{ssl_login, vapor_addr_udp};
use socket::*;
use state::Context;
use message::*;
use vapor::types::*;
use guarantee::GuaranteeLevel;
use {read_all, poll_loop, PollLoopCmd};

pub enum ClientCtl {
   Stop,
}

/// [incomplete] A connection to Vapor.
/// Logs in over SSL then keeps a UDP socket open for client varification and
/// friend communication.
pub struct VaporClient {
   pub thread: thread::JoinHandle<()>,
   pub ctl: Sender<ClientCtl>,
}

impl VaporClient {
   /// Request that the client sends a logout request and stops
   pub fn stop(&self) {
      let _ = self.ctl.send(ClientCtl::Stop);
   }

   /// Create a new vapor client, logging in using the given LoginReq.
   ///
   /// Retruns a error if the SSL connection fails, we receive an unexpected
   /// response from the server, or hte login request fails or times out.
   /// The newly created thread will be stopped if the server sends a
   /// flood warning or a disconnect, or the `stop` function on the
   /// returned `VaporClient` is called.
   /// At the end, a logout request will be sent to the server, and the client
   /// stops without waiting for a response.
   pub fn new(req: LoginReq, port: u16) -> Result<VaporClient, String> {

      // Initial SSL Handshake - Login details are sent over TLS, the server
      // returns a private session ID which we then use to authenticate
      // over the usual UDP based protocol.
      let ssl_resp = match ssl_login(req) {
         Ok(AuthCommand::LoginResp(l)) => l,
         Ok(x) => return Err(format!("Unknown login response: {:?}", x)),
         Err(e) => return Err(format!("SSL error: {}", e)),
      };

      if ssl_resp.id.is_none() {
         match ssl_resp.result {
            Some(e) => return Err(e),
            None => return Err("Login failed".to_string()),
         }
      }

      debug!("ssl_resp: {:?}", ssl_resp);
      let id = ssl_resp.id.unwrap();
      let _client_id = id.id.clone();
      let psid = id.vapor_opts.private_sid.unwrap();

      // Create an altitude UDP socket, the timer, and a channel to control
      // the vapor client, and register them to a mio::Poll.
      let addr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), port));
      let socket = Socket::new(&addr, Context::Client).unwrap();
      let mut timer = mio::timer::Builder::default()
         .tick_duration(Duration::from_secs(5))
         .build();
      let (sx_ctl, rx_ctl) = from_std_channel(mpsc::channel::<ClientCtl>());

      let poll = Poll::new().unwrap();
      poll.register(&socket.rx,
                   mio::Token(1),
                   mio::EventSet::readable(),
                   mio::PollOpt::edge())
         .unwrap();
      poll.register(&timer,
                   mio::Token(2),
                   mio::EventSet::readable(),
                   mio::PollOpt::edge())
         .unwrap();
      poll.register(&rx_ctl,
                   mio::Token(3),
                   mio::EventSet::readable(),
                   mio::PollOpt::edge())
         .unwrap();

      // UDP handshake 1
      // Sends the private session ID we received over SSL over the UDP
      // socket, the server presubaly checks that the packet's from the
      // correct IP, and logs us in.
      info!("Sending UDP login request");
      let udp_login = vapor_msg(Data::Auth(AuthMsg::Request {
                                   private_sid: psid,
                                   t: AuthReqData::Login,
                                }),
                                port);
      socket.sx.send(udp_login).unwrap();

      // Wait for a login response, timeout after five seconds with no reply
      info!("Waiting for login response");
      timer.set_timeout(Duration::from_secs(5), 0).unwrap();

      let res = poll_loop(&poll, |ev: mio::Event| {
         match ev.token().0 {
            // Socket readable:
            1 => {
               read_all(&socket.rx, |msg: Message| {
                  match msg.data {
                     Auth(AuthMsg::Response(AuthCommand::LoginSuccess(_r))) => {
                        // Send confirmation of login
                        info!("Got UDP login response");
                        socket.sx
                           .send(vapor_msg(Data::Auth(AuthMsg::Request {
                                              private_sid: psid,
                                              t: AuthReqData::LoginConfirm,
                                           }),
                                           port))
                           .unwrap();
                        return Ok(PollLoopCmd::Stop);
                     }
                     Auth(AuthMsg::Response(AuthCommand::LoginFail(r))) => {
                        return Err(format!("Login failed: {}", r));
                     }
                     x => {
                        warn!("Got unexpected message type: {:?}", x);
                     }
                  }
                  Ok(PollLoopCmd::Continue)
               })
            }
            // Timer trigger:
            2 => {
               if let Some(_) = timer.poll() {
                  Err("Login request timed out".to_string())
               } else {
                  Ok(PollLoopCmd::Continue)
               }
            }
            _ => Ok(PollLoopCmd::Continue),
         }
      });
      if let Err(e) = res {
         return Err(e);
      }

      let t = thread::Builder::new().name(format!("vapor_client"));
      let thread = t.spawn(move || {
            timer.set_timeout(Duration::from_secs(30), 0).unwrap();
            let res = poll_loop(&poll, |ev: mio::Event| {
               match ev.token().0 {
                  // Socket readable:
                  1 => {
                     read_all(&socket.rx, |msg: Message| {
                        match msg.data {
                           Auth(AuthMsg::Response(AuthCommand::KeepaliveResp(_))) => {
                              debug!("Got keepalive response");
                           }
                           Auth(AuthMsg::Response(AuthCommand::Disconnected(e))) => {
                              if e.check_id(psid) {
                                 return Err(format!("Vapor disconnected: {:?}", e));
                              }
                           }
                           Auth(AuthMsg::Response(AuthCommand::FloodWarning(e))) => {
                              return Err(format!("Got flood warning: {:?}", e));
                           }
                           x => {
                              warn!("Got unexpected message type: {:?}", x);
                           }
                        }
                        Ok(PollLoopCmd::Continue)
                     })
                  }
                  // Timer trigger:
                  2 => {
                     if let Some(_) = timer.poll() {
                        // Send keepalive
                        debug!("Sending keepalive");
                        socket.sx
                           .send(vapor_msg(Data::Auth(AuthMsg::Request {
                                              private_sid: psid,
                                              t: AuthReqData::Keepalive,
                                           }),
                                           port))
                           .unwrap();
                        timer.set_timeout(Duration::from_secs(30), 0)
                           .unwrap();
                     }
                     Ok(PollLoopCmd::Continue)
                  }
                  3 => {
                     read_all(&rx_ctl, |cmd: ClientCtl| {
                        match cmd {
                           ClientCtl::Stop => {
                              info!("Vapor client stopped");
                              return Ok(PollLoopCmd::Stop);
                           }
                        }
                     })
                  }
                  _ => Ok(PollLoopCmd::Continue),
               }
            });

            if let Err(e) = res {
               error!("{}", e);
            }

            info!("Sending logout");
            socket.sx
               .send(vapor_msg(Data::Auth(AuthMsg::Request {
                                  private_sid: psid,
                                  t: AuthReqData::Logout(psid),
                               }),
                               port))
               .unwrap();

            info!("Closing socket");
            socket.stop();
            let _ = socket.thread.join();

         })
         .unwrap();

      Ok(VaporClient {
         thread: thread,
         ctl: sx_ctl,
      })
   }
}

fn vapor_msg(x: Data, port: u16) -> Message {
   Message::new(vapor_addr_udp(), port, GuaranteeLevel::Delivery, x)
}
