use std::io;
use bitstream::*;
use netable::*;
use client::*;
use meta::*;

// =====================================================
// Validation
// =====================================================

use state::Context;

/// A vapor client validation message, a type of message data
#[derive(Debug,Clone)]
pub enum ValidationMsg {
   Request {
      id: ClientID,
      game_id: i32,
   },
   Response(ClientAuth),
}

/// Client validation information
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct ClientAuth {
   pub id: ClientID,
   pub valid: bool,
   pub game_valid: bool,
   pub owns_game: bool,
   pub message: String,
   pub is_beta: bool,
   pub ip_valid: bool,
   pub ban: BanReason,
}}

impl ValidationMsg {
   pub fn write(&self, p: &mut BitStreamVec, state: &mut MsgState) -> Res<()> {
      match *self {
         ValidationMsg::Request { ref id, ref game_id } => {
            try!(p.write_u8(3));
            try!(id.write(p, state));
            p.write_i32(*game_id)
         }
         ValidationMsg::Response(ref auth) => {
            try!(p.write_u8(4));
            auth.write(p, state)
         }
      }
   }
   pub fn read(p: &mut BitStreamVec, state: &mut MsgState) -> Res<Self> {
      match try!(p.read_u8()) {
         4 => Ok(ValidationMsg::Response(try!(ClientAuth::read(p, state)))),
         3 => {
            // The routing header is written twice, so let's ignore that.
            let _ = try!(p.read_u8());
            Ok(ValidationMsg::Request {
               id: try!(ClientID::read(p, state)),
               game_id: try!(p.read_i32()),
            })
         }
         x => {
            return Err(io::Error::new(io::ErrorKind::Other,
                                      format!("Unknown varification message type: {}", x)))
         }
      }
   }
}

// =====================================================
// Authentication
// =====================================================

/// A vapor authorisation command
multimsg!{ AuthCommand, 8, RW;
    0, LoginReq LoginReq;
    1, LoginResp LoginResp;
    2, LoginFail String;
    7, LoginSuccess LoginResp;
    11, Disconnected VaporError;
    23, KeepaliveResp EmptyNet;
    27, FloodWarning VaporError;
}

#[derive(Debug,Clone)]
/// The data in an authorisation request message
pub enum AuthReqData {
   Login,
   Logout(Uuid),
   Keepalive,
   LoginConfirm,
}

/// A vapor authentication message
#[derive(Debug,Clone)]
pub enum AuthMsg {
   Request {
      private_sid: Uuid,
      t: AuthReqData,
   },
   Response(AuthCommand),
}

/// Vapor login request data, part of `AuthCommand::LoginReq`
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct LoginReq{
    pub username:       Option<String>,
    pub password:       Option<String>,
    pub encrypted_pass: Option<ByteArray>,
    pub steam_id:       Option<SteamID>,
    pub steam_nick:     Option<String>,
    pub unk:            Option<ByteArray>,
    pub steam_sync:     Option<SteamSync>,
    pub distributor:    Distributor,
    pub platform:       Platform,
    pub upnp:           bool,
}}

/// Vapor's response to  a login request - much of this data is currently
/// unknown
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct LoginResp{
    pub result:    Option<String>,
    pub logged_in: bool,
    pub id:        Option<PrivClientID>,
    pub authority: Option<String>,
    pub unk1:      bool,
    pub games:     Vec<GameID>,
    pub unk2:      bool,
    pub unk3:      bool,
    pub unk4:      bool,
    pub unk5:      i32,
    pub ban:       BanInfo,
}}

/// Vapor's error format, includes the error message and the users' private
/// ID for authentication
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct VaporError{
    pub message:     String,
    pub private_sid: Uuid,
}}

/// All the client information sent on a sucessful login.
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct PrivClientID{
    pub vapor_opts: VaporSettings,
    pub id:         ClientID,
}}

/// A client's account settings
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct VaporSettings{
    pub username:     String,
    pub private_sid:  Option<Uuid>,
    pub hw_info_time: i64,
    pub logged_in_ms: i64,
    pub no_chat:      bool,
    pub email_opt_in: bool,
    pub chat_filter:  bool,
    pub map_filter:   bool,
    pub is_guest:     bool,
    pub name_filter:  bool,
    pub steam_uid:    Option<i64>,
    pub auth_method:  AuthMethod,
    pub created:      i64,
}}

/// An identifier for a game which uses vapor
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct GameID{
    pub id:      i16,
    pub version: String,
    pub name:    String,
}}

/// Unknown, OnlineConduct, or AdminUpdate
#[derive(Debug,Clone,Copy)]
pub enum BanReason {
   Unknown,
   OnlineConduct,
   AdminUpdate,
}
enum_netable!( BanReason; 2; Unknown = 0, OnlineConduct = 1, AdminUpdate = 2 );

/// Information regarding a possible account ban
#[derive(Debug,Clone,Copy)]
pub struct BanInfo {
   pub is_banned: bool,
   pub expires: i32,
   pub reason: BanReason,
}

/// Windows, Mac, Linux, or Unknown
#[derive(Debug,Clone,Copy)]
pub enum Platform {
   Unknown,
   Windows,
   Mac,
   Linux,
}
enum_netable!( Platform; 2; Unknown = 0, Windows = 1, Mac = 2, Linux = 3 );

/// Nimbly, Steam, or Unknown
#[derive(Debug,Clone,Copy)]
pub enum Distributor {
   Unknown,
   Nimbly,
   Steam,
}
enum_netable!( Distributor; 3; Unknown = 0, Nimbly = 1, Steam = 5 );

/// Vapor, Steam, or Unknown
#[derive(Debug,Clone,Copy)]
pub enum AuthMethod {
   Vapor,
   Steam,
   Unknown,
}
enum_netable!( AuthMethod; 2; Unknown = 2, Vapor = 0, Steam = 1 );

vec_netable!(ByteArray, 20, u8);
#[derive(Debug,Clone,Default)]
pub struct SteamID;
empty_netable!(SteamID);
#[derive(Debug,Clone,Default)]
pub struct SteamSync;
empty_netable!(SteamSync);

impl LoginReq {
   /// Create a login request from a username and password. Steam fields are
   /// left empty, the distributor is set to Nimbly, and the platform
   /// Linux.
   pub fn new(username: &str, pass: &str) -> LoginReq {
      LoginReq {
         username: Some(username.to_owned()),
         password: Some(pass.to_owned()),
         encrypted_pass: None,
         steam_id: None,
         steam_nick: None,
         unk: None,
         steam_sync: None,
         distributor: Distributor::Nimbly,
         platform: Platform::Linux,
         upnp: false,
      }
   }
}

impl AuthMsg {
   pub fn write(&self, p: &mut BitStreamVec, state: &mut MsgState) -> Res<()> {
      match *self {
         AuthMsg::Request { ref private_sid, ref t } => {
            try!(private_sid.write(p, state));
            match *t {
               AuthReqData::Login => {
                  try!(p.write_u8(0u8));
               }
               AuthReqData::Logout(ref id) => {
                  try!(p.write_u8(9u8));
                  try!(id.write(p, state));
               }
               AuthReqData::Keepalive => {
                  try!(p.write_u8(14u8));
               }
               AuthReqData::LoginConfirm => {
                  try!(p.write_u8(24u8));
               }
            }
            Ok(())
         }
         AuthMsg::Response(ref cmd) => {
            try!(cmd.write(p, state));
            Ok(())
         }
      }
   }
   pub fn read(p: &mut BitStreamVec, state: &mut MsgState) -> Res<Self> {
      match state.context {
         Context::Client => Ok(AuthMsg::Response(try!(AuthCommand::read(p, state)))),
         Context::Server => {
            Ok(AuthMsg::Request {
               private_sid: try!(Uuid::read(p, state)),
               t: match try!(p.read_u8()) {
                  0 => AuthReqData::Login,
                  // 9  => { unimplemented!(); }
                  14 => AuthReqData::Keepalive,
                  28 => AuthReqData::LoginConfirm,
                  x => {
                     return Err(io::Error::new(io::ErrorKind::Other,
                                               format!("Unknown auth message type: {}", x)))
                  }
               },
            })
         }
      }
   }
}

impl<T: RW> Netable<T> for BanInfo {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self> {
      match try!(p.read_bool()) {
         false => {
            Ok(BanInfo {
               is_banned: false,
               expires: 0,
               reason: BanReason::Unknown,
            })
         }
         true => {
            Ok(BanInfo {
               is_banned: true,
               expires: try!(p.read_i32()),
               reason: try!(BanReason::read(p, state)),
            })
         }
      }
   }
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      match self.is_banned {
         true => {
            try!(p.write_bool(true));
            try!(p.write_i32(self.expires));
            self.reason.write(p, state)
         }
         false => p.write_bool(false),
      }
   }
}

impl VaporError {
   pub fn check_id(&self, id: Uuid) -> bool {
      match self.private_sid == id {
         true => return true,
         false => {
            warn!("Got vapor error for a different session ID");
            return false;
         }
      }
   }
}

// =====================================================
// Friend Communication
// =====================================================

use std::net::SocketAddr;

multimsg!{ FriendCmd, 8, RW;
    0,  Info FriendInfoMsg;
    1,  Add FriendAdd;
    //8,  IM;
    //10, AddResp;
    12, IMRelay IMRelay;
    27, MultiInfo FriendInfos;
}

#[derive(Debug,Clone)]
pub struct FriendMsg {
   pub data: FriendCmd,
}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct FriendInfos{
    pub uuid: Uuid,
    pub info: Vec<FriendInfo>,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct FriendInfoMsg{
    pub uuid: Uuid,
    pub info: FriendInfo,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct FriendAdd{
    pub username: Option<String>,
    pub uuid:     Option<Uuid>,
    pub message:  Option<String>,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct IMRelay{
    pub addr:     SocketAddr,
    pub message:  String,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct FriendInfo{
    pub blocked:     bool,
    pub declined:    bool,
    pub id:          PlayerID,
    pub sent_invite: bool,
    pub squelched:   bool,
    pub invite_req:  bool,
    pub invite_msg:  Option<String>,
}}

#[derive(Debug,Clone)]
pub enum PlayerID {
   Friend(ClientIDFriend),
   Other(ClientIDBase),
}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct ClientIDFriend{
    pub game_id:      i16,
    pub server_addr:  Option<SocketAddr>,
    pub hosting_chat: bool,
    pub chat_header:  u8,
    pub last_online:  i64,
    pub addr:         Option<SocketAddr>,
    pub logged_in:    bool,
    pub id:           ClientIDBase,
}}

impl FriendMsg {
   pub fn write(&self, p: &mut BitStreamVec, state: &mut MsgState) -> Res<()> {
      self.data.write(p, state)
   }
   pub fn read(p: &mut BitStreamVec, state: &mut MsgState) -> Res<Self> {
      let data = try!(FriendCmd::read(p, state));
      Ok(FriendMsg { data: data })
   }
}

impl<T: RW> Netable<T> for PlayerID {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self> {
      trace!("reading player ID");
      match try!(p.read_bool()) {
         true => {
            trace!("is friend");
            Ok(PlayerID::Friend(try!(ClientIDFriend::read(p, state))))
         }
         false => {
            trace!("not friend");
            Ok(PlayerID::Other(try!(ClientIDBase::read(p, state))))
         }
      }
   }
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      match *self {
         PlayerID::Friend(ref x) => {
            try!(p.write_bool(true));
            x.write(p, state)
         }
         PlayerID::Other(ref x) => {
            try!(p.write_bool(false));
            x.write(p, state)
         }
      }
   }
}
