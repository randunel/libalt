extern crate openssl;

mod types;
mod client;

pub use self::client::*;
pub use self::types::*;

use bitstream::*;
use netable::*;
use self::openssl::ssl::*;
use self::openssl::x509::X509;
use self::openssl::ssl::error::SslError;
use std::fs::File;
use std::net::TcpStream;
use std::net::SocketAddr;
use std::net::Shutdown;
use std::str::FromStr;
use std::io;

/// Try to load the vapor certificate from a `vapor.pem` file.
pub fn vapor_cert<'ctx>() -> Result<X509<'ctx>, SslError> {
   let mut file = match File::open("vapor.pem") {
      Ok(f) => f,
      Err(e) => return Err(SslError::StreamError(e)),
   };
   X509::from_pem(&mut file)
}
/// Get the current vapor SSL address
pub fn vapor_addr_ssl() -> SocketAddr {
   SocketAddr::from_str("216.246.108.250:31373").unwrap()
}
/// Get the current vapor UDP address
pub fn vapor_addr_udp() -> SocketAddr {
   SocketAddr::from_str("216.246.108.250:31383").unwrap()
}

/// Create an SSL stream connected to vapor
pub fn vapor_stream_ssl<'a>() -> Result<SslStream<TcpStream>, SslError> {
   let mut context = try!(SslContext::new(SslMethod::Tlsv1_1));
   try!(context.set_certificate(&try!(vapor_cert())));

   let ssl = try!(Ssl::new(&context));
   let stream = match TcpStream::connect(&vapor_addr_ssl()) {
      Ok(stream) => stream,
      Err(e) => return Err(SslError::StreamError(e)),
   };

   SslStream::connect(ssl, stream)
}

/// Close an SSL stream
pub fn close_stream(s: &SslStream<TcpStream>) -> io::Result<()> {
   s.get_ref().shutdown(Shutdown::Both)
}

/// Login to vapor over SSL
pub fn ssl_login(req: LoginReq) -> Result<AuthCommand, SslError> {
   use std::io::Write;
   fn io<T>(x: io::Result<T>) -> Result<T, SslError> {
      x.map_err(|y| SslError::StreamError(y))
   }

   let s = try!(vapor_stream_ssl());
   let mut write = BitStream::from_stream(s);

   let req = AuthCommand::LoginReq(req);

   let state = &mut MsgState::new();
   try!(io(req.write(&mut write, state)));
   try!(io(write.write_buf()));

   let mut s = write.into_inner();
   try!(io(s.flush()));
   let mut read = BitStream::from_stream(s);

   let resp = try!(io(AuthCommand::read(&mut read, state)));

   try!(io(close_stream(&read.into_inner())));
   Ok(resp)
}
