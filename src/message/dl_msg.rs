use bitstream::*;
use netable::*;

#[derive(Debug,Clone)]
pub enum DlMsg {
   Req {
      id: u64,
      name: String,
   },
   Accept {
      id: u64,
   },
   ReqLost,
   Data,
   Complete,
}

impl DlMsg {
   pub fn write(&self, p: &mut BitStreamVec, _state: &mut MsgState) -> Res<()> {
      match *self {
         DlMsg::Req { id, ref name } => {
            try!(p.write(8, id));
            p.write_str(&name[..])
         }
         DlMsg::Accept { id } => p.write(8, id),
         _ => unimplemented(),
      }
   }
   pub fn read(p: &mut BitStreamVec, _state: &mut MsgState) -> Res<Self> {
      let t = try!(p.read(3));
      Ok(match t {
         0 => {
            DlMsg::Req {
               id: try!(p.read(8)),
               name: try!(p.read_str()),
            }
         }
         1 => DlMsg::Accept { id: try!(p.read(8)) },
         2 => DlMsg::ReqLost,
         3 => DlMsg::Data,
         4 => DlMsg::Complete,
         _ => unreachable!(),
      })
   }
}
