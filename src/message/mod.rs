//! Libalt's representation of an Altitude packet.
//!
//! A message contains a remote address, a local port, delivery guarantee
//! information, and the message data itself.

pub mod dl_msg;
pub mod game_msg;
pub mod info_msg;
pub mod join_msg;
pub mod lan_msg;
pub mod ping_msg;

pub use self::dl_msg::*;
pub use self::game_msg::*;
pub use self::info_msg::*;
pub use self::join_msg::*;
pub use self::lan_msg::*;
pub use self::ping_msg::*;

use std::net::{SocketAddr, ToSocketAddrs};
use guarantee::*;
use bitstream::*;
use netable::*;
use vapor::*;

macro_rules! data{
    ($e:ident; $($header:expr, $name:ident $entity:ty);*;) => {
        #[derive(Debug,Clone)]
        pub enum $e {
            Empty,
            Raw(u8, Vec<u8>),
            $($name($entity)),*
        }

        impl $e{
            pub fn decode(header: u8, p: &mut BitStreamVec, state: &mut MsgState)
            -> Res<$e> {
                let res = match header{
                    $($header => $e::$name(try!(<$entity>::read(p,state)))),*,
                    x => {
                       use std::io::{Error,ErrorKind};
                       return Err(Error::new(ErrorKind::InvalidData,
                                  format!("Unknown routing header {}", x)))
                    }
                };
                trace!("read {:?}",res);
                Ok(res)
            }
            fn header(&self) -> u8 {
                match *self{
                    $e::Empty => 0,
                    $e::Raw(header, _) => header,
                    $($e::$name(_) => $header),*
                }
            }
            fn encode(&self, p: &mut BitStreamVec, state: &mut MsgState) -> Res<()>{
                match *self{
                    $e::Empty => (),
                    $e::Raw(_, ref data) => try!(p.write_bytes(data)),
                    $($e::$name(ref x) => try!(x.write(p,state))),*
                } Ok(())
            }
        }
    };
}

data!{ Data;
   0, Friend FriendMsg;
   1, Auth AuthMsg;
   2, Validation ValidationMsg;
   // 3, ServerList
   // 4, ServerListHandler
   // 5, Versioning
   // 6, FirewallChecker
   7, ServerInfo InfoMsg;
   8, ServerPing PingMsg;
   // 9, GameStorage
   // 10, HardwareReport
   70, Join JoinMsg;
   71, Game GameMsg;
   72, Dl DlMsg;
   73, Ping PingMsg;
   74, Lan LanMsg;
}
pub use self::Data::*;

#[derive(Clone, Debug)]
pub struct Message {
   /// The external address of the sender/recipient
   pub addr: SocketAddr,
   /// The *local* port
   pub port: u16,
   /// Delivery guarantee information, used internaly
   pub guarantee: Guarantee,
   pub data: Data,
}

impl Message {
   /// Create a new message.
   pub fn new<A: ToSocketAddrs>(addr: A, port: u16, g: GuaranteeLevel, data: Data) -> Message {
      let g = Guarantee {
         level: g,
         data: GuaranteeData::None,
      };
      let addr = addr.to_socket_addrs()
         .ok()
         .expect("Failed to parse socket address!")
         .next()
         .expect("No socket address given!");
      Message {
         addr: addr,
         port: port,
         guarantee: g,
         data: data,
      }
   }

   /// Create a message in reply to another.
   ///
   /// Copies the address, local port, and guarantee level to a new message,
   /// adding the new data.
   pub fn reply(&self, data: Data) -> Message {
      Message {
         addr: self.addr.clone(),
         port: self.port,
         guarantee: Guarantee {
            level: self.guarantee.level,
            data: GuaranteeData::None,
         },
         data: data,
      }
   }

   /// Create a Message from raw data.
   pub fn decode(data: Vec<u8>, addr: SocketAddr, port: u16, state: &mut MsgState) -> Res<Message> {
      let mut p = BitStream::from_vec(data);

      let g_level = try!(GuaranteeLevel::read(&mut p, state));
      let header = try!(p.read_u8());
      let g_data = match g_level {
         GuaranteeLevel::Delivery | GuaranteeLevel::Order => {
            try!(GuaranteeData::read(&mut p, state))
         }
         _ => GuaranteeData::None,
      };
      let guarantee = Guarantee {
         level: g_level,
         data: g_data,
      };
      trace!("Decoding packet: {}, {:?}", header, guarantee);
      let data = match guarantee.data {
         GuaranteeData::Open { .. } |
         GuaranteeData::Data { .. } |
         GuaranteeData::None => try!(Data::decode(header, &mut p, state)),
         _ => Empty,
      };

      Ok(Message {
         addr: addr,
         port: port,
         guarantee: guarantee,
         data: data,
      })
   }

   pub fn encode(&self, state: &mut MsgState) -> Res<Vec<u8>> {
      let mut p = BitStream::new();

      try!(self.guarantee.level.write(&mut p, state));
      try!(p.write_u8(self.data.header()));
      try!(self.guarantee.data.write(&mut p, state));
      try!(self.data.encode(&mut p, state));
      try!(p.write_buf());

      Ok(p.into_vec())
   }

   /// Encode the message data to Data::Raw(header, bytes).
   pub fn to_raw(&self, state: &mut MsgState) -> Res<Message> {
      let mut p = BitStream::new();
      let header = self.data.header();
      try!(self.data.encode(&mut p, state));
      try!(p.write_buf());
      let d = p.into_vec();
      Ok(Message {
         data: Data::Raw(header, d),
         addr: self.addr.clone(),
         port: self.port,
         guarantee: self.guarantee.clone(),
      })
   }
}
