use bitstream::*;
use state::Context;
use netable::*;
use server::*;
use meta::*;
use client::*;

#[derive(Debug,Clone)]
pub enum JoinMsg {
   JoinReq(JoinReq),
   JoinResp(JoinResp),
   LoadReq(LoadReq),
   LoadResp(LoadResp),
   Disconnect,
   KeepAlive,
}

impl JoinMsg {
   pub fn write(&self, p: &mut BitStreamVec, state: &mut MsgState) -> Res<()> {
      match *self {
         JoinMsg::JoinReq(ref x) => {
            try!(p.write(2, 0));
            x.write(p, state)
         }
         JoinMsg::JoinResp(ref x) => {
            try!(p.write(2, 0));
            x.write(p, state)
         }
         JoinMsg::LoadReq(ref x) => {
            try!(p.write(2, 1));
            x.write(p, state)
         }
         JoinMsg::LoadResp(ref x) => {
            try!(p.write(2, 1));
            x.write(p, state)
         }
         _ => Ok(()),
      }
   }
   pub fn read(p: &mut BitStreamVec, state: &mut MsgState) -> Res<Self> {
      let t = try!(p.read(2));
      Ok(match state.context {
         Context::Server => {
            match t {
               0 => JoinMsg::JoinReq(try!(JoinReq::read(p, state))),
               1 => JoinMsg::LoadReq(try!(LoadReq::read(p, state))),
               2 => JoinMsg::Disconnect,
               3 => JoinMsg::KeepAlive,
               _ => unreachable!(),
            }
         }
         Context::Client => {
            match t {
               0 => JoinMsg::JoinResp(try!(JoinResp::read(p, state))),
               1 => JoinMsg::LoadResp(try!(LoadResp::read(p, state))),
               2 => JoinMsg::Disconnect,
               3 => JoinMsg::KeepAlive,
               _ => unreachable!(),
            }
         }
      })
   }
}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct JoinReq {
    pub version: Version,
    pub pass:    String,
    pub level:   ClientLevel,
    pub id:      ClientID
}}

#[derive(Debug,Clone)]
pub struct JoinResp {
   pub allow: bool,
   pub reason: Option<String>,
}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct LoadReq{
    pub resource: Resource,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct LoadResp{
    pub counter: u2,
    pub resource: Resource,
    pub conf: ServerConf,
}}

impl<T: RW> Netable<T> for JoinResp {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self> {
      let reason = try!(Option::<String>::read(p, state));
      Ok(JoinResp {
         allow: reason.is_none(),
         reason: reason,
      })
   }
   fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      try!(p.write_bool(self.allow));
      if let Some(ref r) = self.reason {
         try!(p.write_str(&r));
      }
      Ok(())
   }
}
