use bitstream::*;
use netable::*;

#[derive(Debug,Clone)]
pub struct PingMsg {
   pub respond: bool,
   pub id: u32,
}
impl PingMsg {
   pub fn write(&self, p: &mut BitStreamVec, _state: &mut MsgState) -> Res<()> {
      try!(p.write_bool(self.respond));
      try!(p.write_u32(self.id));
      Ok(())
   }
   pub fn read(p: &mut BitStreamVec, _state: &mut MsgState) -> Res<Self> {
      Ok(PingMsg {
         respond: try!(p.read_bool()),
         id: try!(p.read_u32()),
      })
   }
}
