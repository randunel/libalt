use bitstream::*;
use netable::*;
use server::*;

#[derive(Debug,Clone)]
pub enum InfoMsg {
   Request,
   Response(ServerInfo),
   Unknown,
}
use self::InfoMsg::*;

impl InfoMsg {
   pub fn write(&self, p: &mut BitStreamVec, state: &mut MsgState) -> Res<()> {
      match *self {
         Response(ref x) => {
            try!(p.write(1, 0));
            try!(x.write(p, state));
         }
         _ => {}
      }
      Ok(())
   }
   pub fn read(p: &mut BitStreamVec, _state: &mut MsgState) -> Res<Self> {
      if p.len() < 10 {
         Ok(Request)
      } else {
         warn!("got server info response, ignoring.");
         Ok(Unknown)
      }
   }
}
