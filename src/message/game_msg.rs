use bitstream::*;
use netable::*;
use event::Event;
use event::ev_group::*;

#[derive(Debug,Clone)]
pub struct GameMsg {
   pub counter: u2,
   pub seq: u11,
   pub group_id: u10,
   pub ev_groups: Vec<EvGroup>,
   pub nuons: Vec<u64>,
   pub data: Vec<u8>,
   pub data_len: u64,
   pub valid: bool,
}

impl GameMsg {
   pub fn write(&self, p: &mut BitStreamVec, state: &mut MsgState) -> Res<()> {
      try!(self.counter.write(p, state));
      try!(self.seq.write(p, state));
      try!(self.group_id.write(p, state));

      if self.valid {
         try!(p.write(2, self.ev_groups.len() as u64));

         for ev in &self.ev_groups {
            try!(ev.write(p, state));
         }

         try!(p.write(2, 0));
         if self.nuons.len() == 0 {
            try!(p.write_bool(false));
         } else {
            try!(p.write_bool(true));
            try!(p.write(10, self.nuons.len() as u64));
            for n in &self.nuons {
               try!(p.write(10, *n));
            }
            try!(p.write_data(&self.data, self.data_len as u64));
         }
      } else {
         try!(p.write_data(&self.data, self.data_len as u64));
      }
      Ok(())
   }

   pub fn new() -> GameMsg {
      GameMsg {
         counter: u2::from(0),
         seq: u11::from(0),
         group_id: u10::from(0),
         ev_groups: Vec::new(),
         nuons: Vec::new(),
         data: Vec::new(),
         data_len: 0,
         valid: true,
      }
   }
   pub fn read(p: &mut BitStreamVec, state: &mut MsgState) -> Res<Self> {
      let mut valid = true;
      let mut ev_groups = Vec::new();
      let mut nuons = Vec::new();

      let counter = try!(u2::read(p, state));
      let seq = try!(u11::read(p, state));
      let group_id = try!(u10::read(p, state));

      let start_pos = p.pos();

      for _ in 0..try!(p.read(2)) {
         match EvGroup::read(p, state) {
            Ok(group) => ev_groups.push(group),
            Err(e) => {
               valid = false;
               warn!("Invalid packet: {:?}", e);
               break;
            }
         }
      }

      if valid {
         for _ in 0..try!(p.read(2)) {
            info!("missing ev group {}!", try!(p.read(11)));
         }
      }

      if valid && try!(p.read_bool()) {
         let n = try!(p.read(10));
         for _ in 0..n {
            nuons.push(try!(p.read(10)));
         }
      }

      if !valid {
         try!(p.seek(start_pos));
      }
      let len = p.len() * 8 - p.pos();
      let len: u64 = if len > 0 {
         len as u64
      } else {
         0
      };
      let data = try!(p.read_data(len));

      Ok(GameMsg {
         counter: counter,
         seq: seq,
         group_id: group_id,
         ev_groups: ev_groups,
         nuons: nuons,
         data: data,
         data_len: len,
         valid: valid,
      })
   }
   pub fn events<F: Fn(&mut Event)>(&mut self, f: F) {
      for group in &mut self.ev_groups {
         group.events(&f);
      }
   }
}
