use bitstream::*;
use netable::*;
use std::net::SocketAddr;

#[derive(Debug,Clone)]
pub enum LanMsg {
   Request,
   Response(Option<SocketAddr>),
}

impl LanMsg {
   pub fn write(&self, p: &mut BitStreamVec, state: &mut MsgState) -> Res<()> {
      match *self {
         LanMsg::Request => {}
         LanMsg::Response(ref x) => try!(x.write(p, state)),
      }
      Ok(())
   }
   pub fn read(p: &mut BitStreamVec, state: &mut MsgState) -> Res<Self> {
      if p.len() == 2 {
         Ok(LanMsg::Request)
      } else {
         Ok(LanMsg::Response(try!(Option::read(p, state))))
      }
   }
}
