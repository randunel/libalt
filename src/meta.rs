/// Netable structures of a higher level, and those which don't fit in any
/// other category.

use std::fmt;
use std::net::{SocketAddrV4, SocketAddrV6, SocketAddr};
use std::net::{Ipv4Addr, Ipv6Addr};
use std::mem::transmute;

use bitstream::*;
use netable::*;
use state::Context;

/// The game version implemented by this version of altserv.
pub const VERSION: Version = Version {
   major: 0,
   minor: 0,
   patch: 374,
};

/// A Unique identifier. Used for account identifiers and public and private
/// session IDs for vapor. The server is sent a user's vapor (account) ID and
/// their public session ID for varification.
custom_derive!{ #[derive(Clone,Copy,PartialEq,Netable(RW))]
pub struct Uuid{
    pub a: u64,
    pub b: u64
}}

/// An altitude game version
/// Altitude uses java's Version class, but semantic versioning is not
/// followed: only the `patch` field is used, and is currently at 0.0.374.
#[derive(Debug,Clone)]
pub struct Version {
   pub major: i32,
   pub minor: i32,
   pub patch: i32,
}

/// An identifier for a network entity
#[derive(Debug,Clone)]
pub struct NetID {
   id: u16,
   b: bool,
}

/// A game resource - usually a map.
custom_derive!{ #[derive(Debug,Clone,Default,Netable(RW))]
pub struct Resource{
    pub path:  String,
    pub size:  u32,
    pub crc32: u64
}}

#[derive(Debug,Clone,Copy)]
pub enum Team {
   A = 0,
   B,
   Spec,
   Red,
   Blue,
   Green,
   Yellow,
   Orange,
   Purple,
   Azure,
   Pink,
   Brown,
}


impl Uuid {
   pub fn new(a: u64, b: u64) -> Uuid {
      Uuid { a: a, b: b }
   }
}

impl fmt::Debug for Uuid {
   fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "uuid {{ a: {:x}, b: {:x} }}", self.a, self.b)
   }
}

impl<T: RW> Netable<T> for Version {
   fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self> {
      let s = try!(p.read_str());
      let res: Result<Vec<i32>, _> = s.split(".")
         .map(|x| x.parse::<i32>())
         .collect();
      if let Ok(parts) = res {
         Ok(Version {
            major: parts[0],
            minor: parts[1],
            patch: parts[2],
         })
      } else {
         warn!("failed to parse version string, using 0.0.374");
         Ok(Version::new(0, 0, 374))
      }
   }
   fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      let s = format!("{}.{}.{}", self.major, self.minor, self.patch);
      p.write_str(&s[..])
   }
}

impl Version {
   pub fn new(a: i32, b: i32, c: i32) -> Version {
      Version {
         major: a,
         minor: b,
         patch: c,
      }
   }
}

impl<T: RW> Netable<T> for NetID {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self> {
      let id = try!(p.read(10)) as u16;
      let b = if state.context == Context::Server {
         try!(p.read_bool())
      } else {
         false
      };
      Ok(NetID { id: id, b: b })
   }
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      try!(p.write(10, self.id as u64));
      if state.context == Context::Client {
         try!(p.write_bool(self.b));
      }
      Ok(())
   }
}

impl<T: RW> Netable<T> for SocketAddr {
   fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self> {
      if try!(p.read_bool()) {
         let ip = Ipv4Addr::new(try!(p.read_u8()),
                                try!(p.read_u8()),
                                try!(p.read_u8()),
                                try!(p.read_u8()));
         let port = try!(p.read_u32()) as u16;
         Ok(SocketAddr::V4(SocketAddrV4::new(ip, port)))
      } else {
         let ip = Ipv6Addr::new(try!(p.read_u16()),
                                try!(p.read_u16()),
                                try!(p.read_u16()),
                                try!(p.read_u16()),
                                try!(p.read_u16()),
                                try!(p.read_u16()),
                                try!(p.read_u16()),
                                try!(p.read_u16()));
         let port = try!(p.read_u32()) as u16;
         Ok(SocketAddr::V6(SocketAddrV6::new(ip, port, 0, 0)))
      }
   }
   fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      let bytes: Vec<u8>;
      match *self {
         SocketAddr::V4(ref a) => {
            try!(p.write_bool(true));
            bytes = a.ip().octets().to_vec();
         }
         SocketAddr::V6(ref a) => {
            try!(p.write_bool(false));
            bytes = unsafe { transmute::<[u16; 8], [u8; 16]>(a.ip().segments()).to_vec() };
         }
      }
      for b in bytes {
         try!(p.write_u8(b));
      }
      p.write_u32(self.port() as u32)
   }
}

impl<T: RW> Netable<T> for Team {
   fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self> {
      use self::Team::*;
      Ok(match try!(p.read(8)) {
         0 => A,
         1 => B,
         2 => Spec,
         3 => Red,
         4 => Blue,
         5 => Green,
         6 => Yellow,
         7 => Orange,
         8 => Purple,
         9 => Azure,
         10 => Pink,
         11 => Brown,
         _ => Spec,
      })
   }
   fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      p.write(8, *self as u64)
   }
}
