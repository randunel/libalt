use std::net::SocketAddr;
use std::collections::HashMap;
use std::sync::mpsc;
use std::thread;
use std::time::*;
use mio::{PollOpt, EventSet, Poll, udp, Token};
use mio::channel::*;
use mio::timer::*;

use state::*;
use message::*;
use guarantee::*;
use bitstream::*;

/// A UDP socket which encodes and decodes packets using altitude's protocol.
///
/// On creation a new thread is spawned, which listens on the underlying UDP
/// socket, handles packet delivery guarantees, and encodes and decodes packet
/// data into `Message`s. The Socket returned by `new` is used to communicate
/// with this thread, sending and receiving messages and closing the socket
/// when it's no longer needed.
/// Socket also includes an Arc<RWLock<State>> to access and modify the state
/// used in decoding packets (maps, network entity types, etc), see the state
/// module for more on this.
pub struct Socket {
   /// A mio Sender over which to send outgoing messages
   pub sx: Sender<Message>,
   /// A mio receiver to which incoming messages will be sent
   pub rx: Receiver<Message>,
   /// The state needed to decode packets
   pub shared: SharedState,
   /// The join handle to the thread the socket is running on
   pub thread: thread::JoinHandle<()>,
   ctl: Sender<SocketCtl>,
}

enum SocketCtl {
   Stop,
}

impl Socket {
   /// Creates a socket running on a new thread and returns a handle
   /// to the socket.
   pub fn new(addr: &SocketAddr, context: Context) -> Res<Socket> {
      // Create channels
      let (mut sx_in, rx_in) = from_std_channel(mpsc::channel::<Message>());
      let (sx_out, rx_out) = from_std_channel(mpsc::channel::<Message>());
      let (sx_ctl, rx_ctl) = from_std_channel(mpsc::channel::<SocketCtl>());

      // Create bound socket
      info!("Creating UDP socket on {:?}", addr);
      let port = addr.port();
      let _addr = addr.clone();
      let mut socket = try!(udp::UdpSocket::bind(addr));

      let mut state = MsgState {
         context: context,
         shared: SharedState::new(),
      };
      let shared = state.shared.clone();

      // Start the socket on a new thread
      let t = thread::Builder::new().name(format!("msg_parser_{}", port));
      let thread = try!(t.spawn(move || {

         let mut connections = HashMap::<SocketAddr, Connection>::new();
         let mut buffer = [0u8; 2048];

         // Create a timer to update and clean connections
         let mut timer = Builder::default()
            .tick_duration(Duration::from_millis(50))
            .build();

         debug!("Registering events to poll");
         let poll = Poll::new().unwrap();
         poll.register(&rx_out, Token(1), EventSet::readable(), PollOpt::level()).unwrap();
         poll.register(&socket, Token(2), EventSet::readable(), PollOpt::level()).unwrap();
         poll.register(&timer, Token(3), EventSet::readable(), PollOpt::edge()).unwrap();
         poll.register(&rx_ctl, Token(4), EventSet::readable(), PollOpt::edge()).unwrap();

         let update_time = Duration::from_millis(50);
         let clean_time = Duration::from_secs(1);
         timer.set_timeout(update_time, 0).unwrap();
         timer.set_timeout(clean_time, 1).unwrap();

         debug!("Starting socket event loop");
         let _ = ::poll_loop(&poll,
                             &mut |ev: ::mio::Event| {
            match ev.token().0 {
               1 => {
                  ::read_all(&rx_out, |msg: Message| {
                     match msg.guarantee.level {
                        GuaranteeLevel::Order | GuaranteeLevel::Delivery => {
                           match msg.to_raw(&mut state) {
                              Ok(msg) => {
                                 if connections.get(&msg.addr).is_none() {
                                    connections.insert(msg.addr.clone(), Connection::new());
                                 }
                                 let c = connections.get_mut(&msg.addr).unwrap();
                                 let msg = c.send(msg);
                                 send(&mut socket, &msg, &mut state);
                              }
                              Err(e) => error!("Failed to encode message: {}", e),
                           }
                        }
                        _ => send(&mut socket, &msg, &mut state),
                     }
                     Ok(::PollLoopCmd::Continue)
                  })
               }
               2 => {
                  loop {
                     match socket.recv_from(&mut buffer) {
                        Ok(Some((len, addr))) => {
                           let data = buffer[0..len].to_owned();
                           match Message::decode(data, addr.clone(), port, &mut state) {
                              Ok(msg) => {
                                 match msg.guarantee.level {
                                    GuaranteeLevel::Order | GuaranteeLevel::Delivery => {
                                       if connections.get(&addr).is_none() {
                                          connections.insert(addr.clone(), Connection::new());
                                       }
                                       let c = connections.get_mut(&addr).unwrap();
                                       let (r, s) = c.receive(msg, None);
                                       recv_all(&sx_in, r);
                                       send_all(&mut socket, s, &mut state);
                                    }
                                    _ => recv(&mut sx_in, msg),
                                 }
                              }
                              Err(e) => error!("Failed to decode message: {}", e),
                           }
                        }
                        Ok(None) => break,
                        Err(e) => error!("Failed to read from socket: {:?}", e),
                     }
                  }
                  Ok(::PollLoopCmd::Continue)
               }
               3 => {
                  // Timer
                  while let Some(x) = timer.poll() {
                     match x {
                        0 => {
                           timer.set_timeout(update_time, 0).unwrap();
                           let now = Instant::now();
                           for (_, c) in &mut connections {
                              send_all_ref(&mut socket, c.update(now), &mut state);
                           }
                        }
                        _ => {
                           timer.set_timeout(clean_time, 1).unwrap();
                           let mut expired = Vec::new();
                           let now = Instant::now();
                           for (a, c) in &connections {
                              if c.status(now) == Status::Expired {
                                 debug!("removing expired connection for {:?}", a);
                                 expired.push(a.clone());
                              }
                           }
                           for e in expired {
                              connections.remove(&e);
                           }
                        }
                     }
                  }
                  Ok(::PollLoopCmd::Continue)
               }
               4 => {
                  ::read_all(&rx_ctl, |cmd: SocketCtl| {
                     match cmd {
                        SocketCtl::Stop => Ok(::PollLoopCmd::Stop),
                     }
                  })
               }
               x => {
                  warn!("Unknown event token {}", x);
                  Ok(::PollLoopCmd::Continue)
               }
            }
         });
         info!("Socket has been stopped ({})", _addr);
      }));

      Ok(Socket {
         sx: sx_out,
         rx: rx_in,
         shared: shared,
         thread: thread,
         ctl: sx_ctl,
      })
   }

   pub fn stop(&self) {
      if self.ctl.send(SocketCtl::Stop).is_err() {
         error!("Socket control channel is broken");
      }
   }
}

fn send(socket: &mut udp::UdpSocket, msg: &Message, state: &mut MsgState) {
   match msg.encode(state) {
      Ok(data) => {
         if socket.send_to(&data[..], &msg.addr).is_err() {
            error!("Failed to send on socket");
         }
      }
      Err(e) => {
         error!("Failed to encode packet: {}", e);
      }
   }
}

fn send_all(socket: &mut udp::UdpSocket, msg: Vec<Message>, state: &mut MsgState) {
   for m in msg {
      send(socket, &m, state);
   }
}

fn send_all_ref(socket: &mut udp::UdpSocket, msg: Vec<&Message>, state: &mut MsgState) {
   for m in msg {
      send(socket, m, state);
   }
}

fn recv(sx: &Sender<Message>, msg: Message) {
   if let Err(e) = sx.send(msg) {
      error!("Socket failed to transfer received message: {:?}", e);
   }
}

fn recv_all(sx: &Sender<Message>, msg: Vec<Message>) {
   for m in msg {
      recv(sx, m);
   }
}
