/// Netable structures which relate to the client

use netable::*;
use bitstream::*;
use meta::*;
use std::net::SocketAddr;

/// A player number.
/// Unique within a single game, used to identify the player in many
/// server events.
#[derive(Debug,Clone,Copy,PartialEq)]
pub struct PlayerNo {
   pub value: i32,
}

/// A player's level.
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct ClientLevel{
   pub ace:    u5,
   pub level:  u7,
   pub kills:  u32,
   pub deaths: u32,
}}

/// The most basic form of a clientID, includes the nickname, vapor ID,
/// and an unknown identifier.
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct ClientIDBase{
   pub nick:  String,
   pub vapor: Uuid,
   pub unk:   u64,
}}

vec_netable!(ClientSkins, 3, u8);

/// A Client ID as known to a server
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct ClientID {
   // The client's public session ID, used for varification
   pub session_id: Option<Uuid>,
   pub demo_a: i64,
   pub demo_b: u16,
   pub c_points: CommunityPoints,
   pub skins: ClientSkins,
   pub addr: Option<SocketAddr>,
   pub logged_in: bool,
   pub base: ClientIDBase,
}}

/// A user's comunity points, split by the reason the points were given.
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct CommunityPoints{
   pub refer: u16,      // Clicks of the user's refer link
   pub facebook: u16,   // Connected to facebook
   pub refer_buy: u16,  // Refered player bought the game
   pub gifted: u16,     // User gifted the game
   pub admin: u16,      // Points given by an admin
   pub steam_group: u16 // Joined the steam group
}}

/// A Player or Bot's plane configuration; includes the plane type
/// and perks. Each perk is an integer, 0, 1, or 2.
#[derive(Debug,Clone,Default)]
pub struct PlaneSetup {
   plane: PlaneType,
   random: RandomType,
   red_perk: i32,
   green_perk: i32,
   blue_perk: i32,
   skin: i32,
}

/// The type of a plane - part of PlaneSetup.
/// Implements Default, PlaneType::default() returns Loopy.
#[derive(Debug,Clone,Copy, PartialEq, Eq)]
pub enum PlaneType {
   Biplane = 0,
   Bomber,
   Explodet,
   Loopy,
   Miranda,
   Unknown,
}
enum_netable!( PlaneType; 3; Unknown = 5, Biplane = 0, Bomber = 1,
               Explodet = 2, Loopy = 3, Miranda = 4 );

#[derive(Debug,Clone,Copy, PartialEq, Eq)]
pub enum RandomType {
   None = 0,
   Config,
   Full,
}
enum_netable!( RandomType; 2; None = 0, Config = 1, Full = 2 );

impl<T: RW> Netable<T> for PlayerNo {
   fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self> {
      Ok(PlayerNo { value: try!(p.read(8)) as i32 - 2 })
   }
   fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      p.write(8, (self.value + 2) as u64)
   }
}

impl<T: RW> Netable<T> for PlaneSetup {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self> {
      if try!(p.read_bool()) {
         use self::RandomType::*;
         let random = if try!(p.read_bool()) {
            Full
         } else {
            Config
         };
         Ok(PlaneSetup {
            plane: PlaneType::Unknown,
            random: random,
            red_perk: 0,
            green_perk: 0,
            blue_perk: 0,
            skin: 0,
         })
      } else {
         Ok(PlaneSetup {
            plane: try!(PlaneType::read(p, state)),
            random: RandomType::None,
            red_perk: try!(p.read_range(-4, 11)),
            green_perk: try!(p.read_range(-4, 11)),
            blue_perk: try!(p.read_range(-4, 11)),
            skin: try!(p.read_range(0, 255)),
         })
      }
   }
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      if self.random == RandomType::None {
         try!(p.write_bool(false));
         try!(self.plane.write(p, state));
         try!(p.write_range(-4, 11, self.red_perk));
         try!(p.write_range(-4, 11, self.green_perk));
         try!(p.write_range(-4, 11, self.blue_perk));
         try!(p.write_range(0, 255, self.skin));
      } else {
         try!(p.write_bool(true));
         try!(p.write_bool(self.random == RandomType::Full));
      }
      Ok(())
   }
}

impl Default for PlaneType {
   fn default() -> Self {
      PlaneType::Loopy
   }
}

impl Default for RandomType {
   fn default() -> Self {
      RandomType::None
   }
}
