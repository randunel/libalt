use bitstream::*;
use math::*;
use map_vec::*;
use netable::*;
use server::*;

#[derive(Debug,Clone)]
pub struct ServerConfigEv {
   is_compressed: bool,
   conf: ServerConf,
}

use event::DataEv;
pub type PingsEv = DataEv;

map_pos!(ViewPos, 0., 1.);
custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct ViewEv{ pub pos: ViewPos, }}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct PacketCountEv{ pub x: u10, }}


impl<T: RWS> Netable<T> for ServerConfigEv {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self> {
      let is_compressed = try!(p.read_bool());
      if is_compressed {
         return unimplemented();
      }
      let pos = p.pos() + try!(p.read(12));

      let conf = try!(ServerConf::read(p, state));

      if p.pos() != pos {
         warn!("ServerConfigEv length not as expected, maybe a parser bug?");
         try!(p.seek(pos));
      }

      Ok(ServerConfigEv {
         is_compressed: is_compressed,
         conf: conf,
      })
   }
   fn write(&self, _p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      unimplemented!();
   }
}
