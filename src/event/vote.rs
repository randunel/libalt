use netable::*;
use bitstream::*;

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct VoteStartEv{
    pub strings_a: Vec<String>,
    pub ints_a:    Vec<i32>,
    pub strings_b: Vec<String>,
    pub ints_b:    Vec<i32>,
    pub message:   String,
    pub options:   Vec<String>,
    pub bool_a:    bool,
    pub bool_b:    bool,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct VoteCastEv{
    pub message: String,
    pub value: u8,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct VoteEndEv{
    pub message: String,
}}
