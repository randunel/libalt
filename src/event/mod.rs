use bitstream::*;
use client::*;
use entity::Entity;
use map_vec::*;
use math::Vec2f;
use meta::*;
use netable::*;

pub mod ev_group;
pub mod vote;
pub mod meta;

pub use self::vote::*;
pub use self::meta::*;

multimsg!{ Event, 6, RWS;
    0,  NewEntity EntityEv;
    1,  RemoveEntity RemoveEv;
    2,  PlayerInfo PlayerInfoEv;
    3,  TeamSet TeamEv;
    4,  EntityMap EntityMapEv;
    //5, ProjectileHit ProjectileHitEv; // Requires projectile state tracking
    //6, AbilityUse AbilityUseEv; // Requires plane & perk tracking
    //7, Kill KillEv;
    8,  Hit HitEv;
    9,  PlaneSetK PlaneSetKEv;
    10, RemovePlane RemovePlaneEv;
    11, PowerupCollide PowerupCollideEv;
    12, PowerupGet PowerupGetEv;
    13, Chat ChatEv;
    14, Emp EmpEv;
    15, Unk15 Unk15Ev;
    16, Unk16 Unk16Ev;
    17, Unk17 Unk17Ev;
    18, Pings PingsEv;
    19, GameModeStatus GameModeStatusEv;
    20, GameMode GameModeEv;
    21, ServerConfig ServerConfigEv;
    //22, Unknown;
    //23, Unknown;
    //24, Unknown;
    25, Stats StatsEv;
    //26, Unknown;
    27, View ViewEv;
    //28, Unknown;
    //29, Unknown;
    30, Command CommandEv;
    31, CmdResp CmdRespEv;
    32, VoteStart VoteStartEv;
    //33, ReqServerChange ReqServerChangeEv;
    34, VoteEnd VoteEndEv;
    35, VoteCast VoteCastEv;
    36, PacketCount PacketCountEv;
    //37, Unknown;
    38, SpawnReq SpawnReqEv;
    39, SpawnResp SpawnRespEv;
    //40, Unknown;
    //41, Unknown;
    //42, BallUpdate BallUpdateEv;
    //43, Unknown;
}

pub type GameModeEv = DataEv;
pub type GameModeStatusEv = DataEv;
pub type StatsEv = DataEv;

#[derive(Debug,Clone)]
pub struct DataEv {
   data: Vec<u8>,
}

impl<T: RW> Netable<T> for DataEv {
   fn read(p: &mut BitStream<T>, _state: &mut MsgState) -> Res<Self> {
      let len = try!(p.read(12));
      Ok(GameModeEv { data: try!(p.read_bytes(len)) })
   }
   fn write(&self, p: &mut BitStream<T>, _state: &mut MsgState) -> Res<()> {
      try!(p.write(12, self.data.len() as u64));
      p.write_bytes(&self.data)
   }
}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct Unk15Ev{
    pub entity: NetID,
    pub target: NetID,
    pub value:  u12,
}}

srange_netable!(Unk16Value, -25., 25., 10.);
map_pos!(Unk16Pos, 50., 1.);

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct Unk16Ev{
    pub entity: NetID,
    pub target: NetID,
    pub value:  Unk16Value,
    pub pos:    Unk16Pos,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct Unk17Ev{
    pub randa:  NetID,
    pub target: NetID,
    pub b:      bool,
    pub angle:  Angle,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct RemovePlaneEv{
    pub plane: NetID,
    pub remove_powerup: bool,
}}

srange_netable!(PlaneK, -15., 15., 8.5);

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct PlaneKVec{ pub x: PlaneK, pub y: PlaneK }}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct PlaneSetKEv{
    pub plane: NetID,
    pub k: PlaneKVec,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct _ProjectileHitEv{
    pub entity: NetID,
    pub target: NetID,
    // If the projecile is a grenade or randa shot, more data
    // is included here.
}}

srange_netable!(HitDamage, 0., 3276., 10.);
srange_netable!(HitAngle, -180., 180., 0.04347826);

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct HitEv{
    pub entity: NetID,
    pub target: NetID,
    pub damage: HitDamage,
    pub angle:  HitAngle,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct EntityMapEv{
    pub a: u10, pub b: u10
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct PowerupCollideEv{
    pub plane: NetID,
    pub powerup: NetID,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct PowerupGetEv{
    pub plane: NetID,
    pub powerup: NetID,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct CommandEv{
    pub command: String,
}}


range_netable!(SpawnTimeout, 0, 450);
map_pos!(SpawnPos, 50., 1.);

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct SpawnRespEv{
    pub team:    Team,
    pub setup:   PlaneSetup,
    pub pos:     SpawnPos,
    pub angle:   Angle,
    pub reason:  String,
    pub timeout: SpawnTimeout,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct SpawnReqEv{
    pub setup:  PlaneSetup,
    pub random: RandomType,
    pub team:   Team,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct EntityEv{
    pub is_a: bool,
    pub id: u10,
    pub entity: Entity,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct RemoveEv{
    pub id: u10,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct TeamEv{
    pub player: PlayerNo,
    pub team: Team,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct CmdRespEv{
    pub command: String,
    pub result:  String,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct PlayerInfoEv{
    pub join:   bool,
    pub id:     PlayerNo,
    pub reason: Option<String>,
    pub nick:   String,
    pub uuid:   Uuid,
    pub unk1:   i64,
    pub setup:  PlaneSetup,
    pub team:   Team,
    pub level:  u6,
    pub ace:    u8,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct EmpEv{
    pub player:  PlayerNo,
    pub target:  NetID,
    pub unk1:    u12,
    pub unk2:    u7,
}}

custom_derive!{ #[derive(Debug,Clone,Netable(RW))]
pub struct ChatEv{
    pub player:  PlayerNo,
    pub is_team: bool,
    pub msg:     String,
}}

/// reads a truncated sequencing number with the given length in bits to the
/// closest original number, given the last known sequence received.
pub fn get_seq(mut seq: u64, len: u64, prev: u64) -> u64 {
   let a = 1u64 << len;
   while seq < prev {
      seq += a
   }
   if seq > prev + a / 2 && seq > a {
      seq -= a
   }
   seq
}
