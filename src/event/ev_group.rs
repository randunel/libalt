use bitstream::*;
use netable::*;
use event::Event;

#[derive(Debug,Clone)]
pub struct EvGroup {
   pub id: u16,
   pub events: Vec<Event>,
   pub b: bool,
}

impl EvGroup {
   pub fn new(id: u16) -> EvGroup {
      EvGroup {
         id: id,
         events: Vec::new(),
         b: false,
      }
   }
   pub fn events<F: Fn(&mut Event)>(&mut self, f: F) {
      for ev in &mut self.events {
         f(ev);
      }
   }
}

impl<T: RWS> Netable<T> for EvGroup {
   fn read(p: &mut BitStream<T>, state: &mut MsgState) -> Res<Self> {
      let id = try!(p.read(10)) as u16;
      let mut events = Vec::new();

      let len = try!(p.read(11));
      let end_pos = p.pos() + len;

      let b = try!(p.read_bool());
      if b {
         return unimplemented();
      } else {
         while p.pos() < end_pos {
            events.push(try!(Event::read(p, state)));
         }
      }
      try!(p.seek(end_pos));
      Ok(EvGroup {
         id: id,
         events: events,
         b: b,
      })
   }
   fn write(&self, p: &mut BitStream<T>, state: &mut MsgState) -> Res<()> {
      try!(p.write(10, self.id as u64));

      let mut p2 = BitStreamVec::new();

      if self.b {
         unimplemented!();
      } else {
         try!(p2.write_bool(false));
         for e in &self.events {
            try!(e.write(&mut p2, state));
         }
      }
      let bits = p2.pos();
      try!(p2.write_buf());
      let data = p2.into_vec();

      try!(p.write(11, bits as u64));
      try!(p.write_data(&data, bits));
      Ok(())
   }
}
