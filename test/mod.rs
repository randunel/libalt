#[macro_use] extern crate log;
extern crate libalt;
extern crate logger;
extern crate bitstream;
extern crate mio;

mod guarantee;
mod socket;
mod vapor;

mod ports {
    use std::net::SocketAddr;
    use std::str::FromStr;
    use std::sync::atomic::{AtomicUsize, ATOMIC_USIZE_INIT};
    use std::sync::atomic::Ordering::SeqCst;

    // Helper for getting a unique port for the task run
    static mut NEXT_PORT: AtomicUsize = ATOMIC_USIZE_INIT;
    const FIRST_PORT: usize = 27300;

    fn next_port() -> usize {
        unsafe {
            // If the atomic was never used, set it to the initial port
            NEXT_PORT.compare_and_swap(0, FIRST_PORT, SeqCst);

            // Get and increment the port list
            NEXT_PORT.fetch_add(1, SeqCst)
        }
    }

    pub fn any() -> SocketAddr {
        let s = format!("0.0.0.0:{}", next_port());
        FromStr::from_str(&s).unwrap()
    }
}
