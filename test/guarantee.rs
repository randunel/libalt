use libalt::guarantee::*;
use libalt::message::*;
use std::net::SocketAddr;
use std::str::FromStr;

fn guarantee_msg(g: Guarantee) -> Message {
    Message{
        addr:      SocketAddr::from_str("0.0.0.0:0").unwrap(),
        port:      0,
        guarantee: g,
        data:      Empty,
    }
}

fn data_msg(d: Data) -> Message {
    Message{
        addr:      SocketAddr::from_str("0.0.0.0:0").unwrap(),
        port:      0,
        guarantee: Guarantee{level: GuaranteeLevel::Order, data: GuaranteeData::None},
        data:      d
    }
}


#[test]
fn receive_no_open(){
    let _ = ::logger::init();
    let mut con = Connection::new();

    let (r,mut s) = con.receive(guarantee_msg(Guarantee{
        level: GuaranteeLevel::Order,
        data: GuaranteeData::Data{ seq: 2 }
        }).upcast(), None);

    assert_eq!(r.len(), 0);
    assert_eq!(s.remove(0),
               guarantee_msg(Guarantee{
                   level: GuaranteeLevel::Order,
                   data: GuaranteeData::OpenAck{ id: 1 },
               }));
}

#[test]
fn receive_open(){
    let _ = ::logger::init();
    let mut con = Connection::new();

    let (mut r,mut s) = con.receive(guarantee_msg(Guarantee{
        level: GuaranteeLevel::Order,
        data: GuaranteeData::Open{
            id: 42,
            is_open: false,
            data: Box::new( GuaranteeData::Data{ seq: 1 } )
        }}, None));

    assert_eq!(r.remove(0), data_msg(Empty));
    assert_eq!(s.remove(0),
               guarantee_msg(Guarantee{
                   level: GuaranteeLevel::Order,
                   data: GuaranteeData::OpenAck{ id: 42 },
               }));
    assert_eq!(s.remove(0),
               guarantee_msg(Guarantee{
                   level: GuaranteeLevel::Order,
                   data: GuaranteeData::DataAck{ seq: 1 },
               }));

    let (mut r,mut s) = con.receive(guarantee_msg(Guarantee{
        level: GuaranteeLevel::Order,
        data: GuaranteeData::Data{ seq: 2 }
        }), None);

    assert_eq!(r.remove(0), data_msg(Empty));

    assert_eq!(s.remove(0),
               guarantee_msg(Guarantee{
                   level: GuaranteeLevel::Order,
                   data: GuaranteeData::DataAck{ seq: 2 },
               }));

    let (r,_) = con.receive(guarantee_msg(Guarantee{
        level: GuaranteeLevel::Order,
        data: GuaranteeData::Data{ seq: 4 }
        }), None);

    assert_eq!(r.len(), 0);

    let (mut r,_) = con.receive(guarantee_msg(Guarantee{
        level: GuaranteeLevel::Order,
        data: GuaranteeData::Data{ seq: 3 }
        }), None);

    assert_eq!(r.remove(0), data_msg(Empty));
    assert_eq!(r.remove(0), data_msg(Empty));
}
