use libalt::socket::*;
use libalt::state::Context;

#[test]
fn socket_start(){
    let _ = ::logger::init();
    let addr = ::ports::any();
    let socket = Socket::new(&addr, Context::Client, default_decoder).unwrap();

    socket.stop();
    socket.thread.join().unwrap();
}
