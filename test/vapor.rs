use libalt::vapor::*;
use std::fs::File;
use libalt::netable::*;
use bitstream::*;

fn from_file(file: &str) -> AuthCommand {
   let file = File::open(file).unwrap();
   let state = &mut MsgState::new();
   AuthCommand::read(&mut BitStream::from_stream(file), state).unwrap()
}

#[test]
fn req_from_file() {
   from_file("request.bin");
}
#[test]
fn resp_from_file() {
   from_file("response.bin");
}

#[test]
fn connect_ssl() {
   match vapor_stream_ssl() {
      Ok(s) => close_stream(&s).unwrap(),
      Err(e) => panic!("{}", e),
   }
}

#[test]
fn test_ssl_login() {
   ssl_login(LoginReq::new("chatbot@xa1.uk", "hunter2")).unwrap();
}
